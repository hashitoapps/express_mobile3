webpackJsonp([12],{

/***/ 327:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(736);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomeModule = (function () {
    function HomeModule() {
    }
    return HomeModule;
}());
HomeModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* Home */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* Home */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* Home */]
        ]
    })
], HomeModule);

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 736:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_departamentos_factory__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_search_factory__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_perfil_factory__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_toast__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_rootScope__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_message_service__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_ui_auth__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_ui_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_ui_auth__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Home; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var $rootScope = __WEBPACK_IMPORTED_MODULE_6__app_rootScope__["a" /* default */];
/**
 * Generated class for the Home page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Home = (function () {
    function Home(navCtrl, navParams, menu, departamentosFactory, userFactory, searchFactory, toast, messageService, auth) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.departamentosFactory = departamentosFactory;
        this.userFactory = userFactory;
        this.searchFactory = searchFactory;
        this.toast = toast;
        this.messageService = messageService;
        this.auth = auth;
        this.paises = [];
        this.paisSeleccionado = {};
        this.menu.swipeEnable(true);
        this.categoria = null;
        this.categorias = [];
        this.variedades = [];
        this.subcategorias = [];
        this.banners = [];
        this.municipioId = null;
        this.departamentos = null;
        this.departamentoId = null;
        this.Departamentos = [];
        this.descripcion = '';
        this.usrApp = this.auth.getPayload().sub;
        this.subscription = this.messageService.getMessage().subscribe(function (message) {
            _this.message = message;
            console.log(message);
            _this.CargarDepartamentos();
        });
        this.userFactory
            .CargarBanners()
            .subscribe(function (res) {
            _this.banners = res.rows;
            console.log('banners: ', _this.banners);
        }, function (err) {
            console.log('error: ', err);
        });
        this.departamentosFactory
            .CargarDepartamentos($rootScope.user.Pai.Id.toString())
            .subscribe(function (res) {
            console.log(res);
            _this.Departamentos = res;
            $rootScope.departamentos = _this.Departamentos;
            console.log('$rootScope Home: ', $rootScope);
        }, function (err) {
            console.log('error on local data saved');
        });
        this.searchFactory
            .LoadCategories()
            .subscribe(function (res) {
            console.log(res);
            _this.categorias = res.rows;
            //vm.subcategorias = vm.categorias[0].SubCategoria;
            _this.subcategorias = _this.categorias;
            _this.CargarPaises();
        }, function (err) {
            console.log('error');
        });
    }
    Home.prototype.onDepChange = function ($event) {
        console.log($event);
    };
    Home.prototype.CargarVariedades = function () {
        var cat = [].filter.call(this.subcategorias, function (sub) {
            return sub.Id /*== this.subcategoria*/;
        });
        if (cat.length > 0) {
            this.variedades = cat[0].Variedads;
        }
        else {
            this.variedades = [];
        }
        this.variedad = cat[0].Variedads[0];
        console.log("variedad" + this.variedad);
    };
    ;
    Home.prototype.Buscar = function () {
        console.log("subcategoria", this.subcategoria);
        console.log("descripcion", this.descripcion);
        console.log("departamentoId", this.departamentoId);
        console.log("municipioId", this.municipioId);
        if (this.municipioId != null) {
            this.navCtrl.push('Results', {
                IdCategoria: this.categorias[0].Id,
                IdSubCategoria: this.subcategoria,
                IdVariedad: this.variedad.Id,
                Descripcion: this.descripcion,
                IdMunicipio: this.municipioId
            });
        }
        else {
            this.toast.show('Por favor seleccione una ciudad.', '2500', 'bottom').subscribe(function (toast) {
                console.log(toast);
            });
            /*ionicToast.show('Por favor seleccione una ciudad.', 'bottom', false, 2500);*/
        }
    };
    Home.prototype.CargarDepartamentos = function () {
        var _this = this;
        this.departamentosFactory
            .CargarDepartamentos($rootScope.user.Pai.Id.toString())
            .subscribe(function (res) {
            console.log(res);
            _this.Departamentos = res;
            $rootScope.departamentos = _this.Departamentos;
            console.log('$rootScope Home: ', $rootScope);
        }, function (err) {
            console.log('error on local data saved');
        });
    };
    Home.prototype.CargarMunicipios = function () {
        var _this = this;
        var departamento = [].filter.call(this.Departamentos, function (dep) {
            return dep.Id == _this.departamentoId;
        });
        if (departamento.length > 0) {
            this.Municipios = departamento[0].Municipios;
        }
        else {
            this.Municipios = [];
        }
        //console.log(vm.Municipios);
        //setTimeout(function(){
        console.log(this.Municipios[0].Id);
        this.municipioId = this.Municipios[0].Id.toString();
        //},1)
    };
    ;
    Home.prototype.CargarPaises = function () {
        var _this = this;
        this.departamentosFactory
            .CargarPaises()
            .subscribe(function (result) {
            console.log("result" + result.rows);
            _this.paises = result.rows;
            _this.paisSeleccionado = $rootScope.user.Pai.Id;
            //console.log("seleccionado:"+this.paisSeleccionado);
        }, function (err) { return console.log("error", err); });
    };
    Home.prototype.CambiarPais = function () {
        var _this = this;
        console.log(this.paisSeleccionado);
        this.departamentoId = null;
        this.municipioId = null;
        this.departamentosFactory
            .CargarDepartamentos(this.paisSeleccionado)
            .subscribe(function (res) {
            console.log(res);
            _this.Departamentos = res;
            $rootScope.departamentos = _this.Departamentos;
            console.log('$rootScope Home: ', $rootScope);
        }, function (err) {
            console.log('error on local data saved');
        });
    };
    Home.prototype.IrACitas = function () {
        this.navCtrl.push('MisreservasPage', { usuario: this.usrApp.Id });
    };
    return Home;
}());
Home = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/home/home.html"*/'<!--\nGenerated template for the Home page.\n\nSee http://ionicframework.com/docs/components/#navigation for more info on\nIonic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="prevenir">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Grupo Prevenir Express</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-slides>\n    <ion-slide *ngFor="let img of banners">\n      <div class="box blue">\n        <img [src]="img.Ruta" alt="Imagen">\n      </div>\n    </ion-slide>\n  </ion-slides>\n  <!-- <ion-slides initialSlide="1" autoplay="4000" loop>\n    <ion-slide>\n      <div class="box blue">\n        <img src="http://www.prevenirexpress.com/assets/images/1920/atardecer.jpg">\n      </div>\n    </ion-slide>\n    <ion-slide>\n      <div class="box yellow">\n        <img src="http://www.prevenirexpress.com/assets/images/1920/flor.jpg">\n      </div>\n    </ion-slide>\n    <ion-slide>\n      <div class="box pink">\n        <img src="http://www.prevenirexpress.com/assets/images/1920/respirar.jpg">\n      </div>\n    </ion-slide>\n  </ion-slides> -->\n  <div padding>\n    <button ion-button round full color="danger" (click)="IrACitas()">\n      <ion-icon name="search"></ion-icon>&nbsp;Mis Citas\n    </button>\n    <h1 class="prevenir-title" text-center>Encuentra el servicio que estas buscando</h1>\n    <form>\n      <ion-list>\n        <ion-item>\n          <ion-label>Categoría</ion-label>\n          <ion-select [(ngModel)]="subcategoria" [ngModelOptions]="{standalone: true}" (ngModelChange)="CargarVariedades()">\n            <ion-option disabled value selected >Selecciona una</ion-option>\n            <ion-option *ngFor="let cat of subcategorias" value="{{ cat.Id }}">{{ cat.Nombre }}</ion-option>\n          </ion-select>\n        </ion-item>\n        <ion-item>\n          <ion-input\n            type="text"\n            [(ngModel)]="descripcion"\n            placeholder="¿Que estas buscando?"\n            [ngModelOptions]="{standalone: true}"\n          ></ion-input>\n        </ion-item>\n      </ion-list>\n      <h1 class="prevenir-title" text-center>¿Donde quieres ser atendido?</h1>\n      <ion-list>\n\n        <ion-list>\n        <ion-item>\n          <ion-select [(ngModel)]="paisSeleccionado" [ngModelOptions]="{standalone: true}" (ngModelChange)="CambiarPais()">\n            <ion-option disabled value selected >Selecciona una</ion-option>\n            <ion-option *ngFor="let p of paises" value="{{ p.Id }}">{{ p.Nombre }}</ion-option>\n          </ion-select>\n          </ion-item>\n          <ion-item>\n            <ion-label>Departamento</ion-label>\n            <ion-select [(ngModel)]="departamentoId" [ngModelOptions]="{standalone: true}" (ngModelChange)="CargarMunicipios()">\n              <ion-option disabled value selected >Selecciona una</ion-option>\n              <ion-option  *ngFor="let dep of Departamentos" value="{{ dep.Id }}" >{{ dep.Nombre }}</ion-option>\n            </ion-select>\n          </ion-item>\n          <ion-item>\n            <ion-label>Ciudad</ion-label>\n            <ion-select [(ngModel)]="municipioId" [ngModelOptions]="{standalone: true}">\n              <ion-option disabled value selected >Selecciona una</ion-option>\n              <ion-option *ngFor="let mun of Municipios" value="{{ mun.Id }}">{{ mun.Nombre }}</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-list>\n\n          <button ion-button round full color="danger" (click)="Buscar()">\n            <ion-icon name="search"></ion-icon>&nbsp;Buscar proveedores\n          </button>\n\n      </ion-list>\n    </form>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/home/home.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_departamentos_factory__["a" /* DepartamentosFactory */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_departamentos_factory__["a" /* DepartamentosFactory */],
        __WEBPACK_IMPORTED_MODULE_4__providers_perfil_factory__["a" /* UserFactory */],
        __WEBPACK_IMPORTED_MODULE_3__providers_search_factory__["a" /* SearchFactory */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_toast__["a" /* Toast */],
        __WEBPACK_IMPORTED_MODULE_7__providers_message_service__["a" /* MessageService */],
        __WEBPACK_IMPORTED_MODULE_8_ng2_ui_auth__["AuthService"]])
], Home);

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=12.main.js.map