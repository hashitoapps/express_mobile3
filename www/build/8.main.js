webpackJsonp([8],{

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__publicacionesusuario__ = __webpack_require__(740);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicacionPageModule", function() { return PublicacionPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PublicacionPageModule = (function () {
    function PublicacionPageModule() {
    }
    return PublicacionPageModule;
}());
PublicacionPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__publicacionesusuario__["a" /* PublicacionesUsuarioPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__publicacionesusuario__["a" /* PublicacionesUsuarioPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__publicacionesusuario__["a" /* PublicacionesUsuarioPage */]
        ]
    })
], PublicacionPageModule);

//# sourceMappingURL=publicacionesusuario.module.js.map

/***/ }),

/***/ 740:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_ui_auth__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_ui_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_ui_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_productos_factory__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_toast__ = __webpack_require__(230);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicacionesUsuarioPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PublicacionesUsuarioPage = (function () {
    function PublicacionesUsuarioPage(navCtrl, navParams, productosFactory, loadingCtrl, auth, toast, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.productosFactory = productosFactory;
        this.loadingCtrl = loadingCtrl;
        this.auth = auth;
        this.toast = toast;
        this.alertCtrl = alertCtrl;
        this.page = 0;
        this.pageSize = 10;
        this.moredata = true;
        this.resultados = 0;
        this.publicaciones = [];
        this.usuario = {};
        //ionicToast.show("Usuario:"+$state.params.Id, 'bottom', false, 2500);
        this.usuario.Id = this.navParams.get('Id');
        this.usuario.Nombre = this.navParams.get('Nombre');
        this.usuario.Proveedor = this.navParams.get('Proveedor');
        this.usuario.NombreEmpresa = this.navParams.get('NombreEmpresa');
        if (this.usuario.Proveedor == "true") {
            this.usuario.Nit = "NIT: " + this.navParams.get('Nit');
        }
        console.log("proveedor:" + this.navParams.get('Proveedor'));
        console.log("nempersa:" + this.navParams.get('NombreEmpresa'));
        console.log("nit:" + this.navParams.get('Nit'));
        this.cargarSiguientePagina(null);
    }
    PublicacionesUsuarioPage.prototype.RefrescarInformacion = function (ionRefresher) {
        var _this = this;
        this.page = 1;
        this.publicaciones = [];
        this.cargarPagina()
            .subscribe(function (result) {
            var pub = result.rows;
            _this.moredata = pub.length == _this.pageSize;
            _this.resultados = result.count;
            _this.publicaciones = _this.publicaciones.concat(pub);
            ionRefresher.complete();
        }, function (err) {
            console.log(err);
        });
    };
    PublicacionesUsuarioPage.prototype.cargarPublicacion = function (Id) {
        this.navCtrl.push('PublicacionPage', { Id: Id });
    };
    PublicacionesUsuarioPage.prototype.cargarPagina = function () {
        //state tiene el Id del usuario de la publicacion
        return this.productosFactory
            .LoadPublicationListByUser(this.usuario.Id, this.page, this.pageSize, 'createdAt', 'DESC', '');
    };
    ;
    PublicacionesUsuarioPage.prototype.cargarSiguientePagina = function (infiniteScroll) {
        var _this = this;
        this.page += 1;
        this.cargarPagina()
            .subscribe(function (result) {
            var pub = result.rows;
            _this.moredata = pub.length == _this.pageSize;
            _this.resultados = result.count;
            _this.publicaciones = _this.publicaciones.concat(pub);
            console.log("publicaciones:" + _this.publicaciones);
            if (!!infiniteScroll) {
                infiniteScroll.complete();
            }
            console.log(result.count);
        }, function (err) {
            console.log(err);
        });
    };
    ;
    return PublicacionesUsuarioPage;
}());
PublicacionesUsuarioPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-publicacionesusuario',template:/*ion-inline-start:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/publicacionesusuario/publicacionesusuario.html"*/'<ion-header>\n\n  <ion-navbar color="prevenir">\n    <ion-title>Listado publicaciones</ion-title>\n  </ion-navbar>\n\n</ion-header>\n<ion-content padding>\n  \n\n<ion-refresher (ionRefresh)="RefrescarInformacion($event)">\n    <ion-refresher-content\n    pulling-text="Arrastra para actualizar...">  \n  </ion-refresher-content>\n</ion-refresher>\n    \n\n\n    <div class="spacer" style="height: 5px;"></div>\n    <h1 class="prevenir-title" text-center>Más publicaciones del usuario</h1>\n    <h5 class="prevenir-subtitle" text-center>{{usuario.NombreEmpresa }}</h5>\n    <h5 class="prevenir-subtitle" text-center>{{usuario.Nit}}</h5>\n    <h5 class="prevenir-subtitle" text-center>{{usuario.Nombre }}</h5>\n    \n\n\n    <ion-list>\n      <ion-card *ngFor="let pub of publicaciones">\n        <ion-item>   \n\n          <h2 class="titulo">{{ pub.Nombre }}</h2>\n          <h3 class="precio-desc"><ion-icon name="thumbsup"></ion-icon>Descuento <small>&nbsp;{{ pub.PorcentajePromo }}&nbsp;%</small></h3>\n          <h3 class="precio"><ion-icon name="thumbsdown"></ion-icon>Particulares <small>&nbsp;{{ pub.Precio | number:0 }}&nbsp;</small></h3>\n          <h3 class="precio-desc"><ion-icon name="thumbsup"></ion-icon>Prevenir Express {{ pub.PrecioDesc | number:0 }}</h3>\n          <p class="descripcion-corta">{{ pub.DescripcionCorta }}</p><br>\n\n          <button ion-button icon-only (click)="cargarPublicacion({Id: pub.Id})">\n            <ion-icon name="eye"></ion-icon>\n          </button>\n        </ion-item>\n      </ion-card>\n    </ion-list>\n    <ion-infinite-scroll (ionInfinite)="cargarSiguientePagina($event)" *ngIf="moredata">\n       <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </ion-content>\n\n'/*ion-inline-end:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/publicacionesusuario/publicacionesusuario.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_productos_factory__["a" /* ProductosFactory */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ng2_ui_auth__["AuthService"],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_toast__["a" /* Toast */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* AlertController */]])
], PublicacionesUsuarioPage);

//# sourceMappingURL=publicacionesusuario.js.map

/***/ })

});
//# sourceMappingURL=8.main.js.map