webpackJsonp([13],{

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contactenos__ = __webpack_require__(734);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactenosModule", function() { return ContactenosModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ContactenosModule = (function () {
    function ContactenosModule() {
    }
    return ContactenosModule;
}());
ContactenosModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__contactenos__["a" /* Contactenos */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contactenos__["a" /* Contactenos */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__contactenos__["a" /* Contactenos */]
        ]
    })
], ContactenosModule);

//# sourceMappingURL=contactenos.module.js.map

/***/ }),

/***/ 734:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_ui_auth__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_ui_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_ui_auth__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contactenos; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ContactenosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Contactenos = (function () {
    function Contactenos(navCtrl, navParams, $auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.$auth = $auth;
    }
    return Contactenos;
}());
Contactenos = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-contactenos',template:/*ion-inline-start:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/contactenos/contactenos.html"*/'<ion-header>\n  <ion-navbar color="prevenir">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Contactenos</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="logo-prevenir" text-center margin-vertical>\n    <img src="assets/img/logo-grande-negro.png">\n  </div>\n  <ion-card margin-top>\n    <ion-card-header class="prevenir-title">\n      <h1 text-center >CONTACTENOS</h1>\n    </ion-card-header>\n\n    <ion-card-content>\n      <p margin-bottom>El objetivo de <strong>Grupo Prevenir Express SAS</strong>\n        es proveer soluciones integrales que faciliten y agilicen la toma de decisiones importantes para nuestros suscriptores, \n        en la adquisición de productos o servicios de las diferentes entidades.\n      </p>\n      <ion-list>\n        <ion-item no-padding>\n          <ion-icon name="locate"></ion-icon>&nbsp;\n          Centro Comercial Los Andes <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Local 214 - San Juan de Pasto\n        </ion-item>\n        <ion-item no-padding>\n          <ion-icon name="phone-portrait"></ion-icon>&nbsp;\n          Teléfono: <a href="call:+573104351937">+57 3104351937</a>\n        </ion-item>\n        <ion-item no-padding>\n          <ion-icon name="mail"></ion-icon>&nbsp;\n          Correo<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:clientes@prevenirexpress.co">clientes@prevenirexpress.com</a>      \n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/contactenos/contactenos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ng2_ui_auth__["AuthService"]])
], Contactenos);

//# sourceMappingURL=contactenos.js.map

/***/ })

});
//# sourceMappingURL=13.main.js.map