webpackJsonp([10],{

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__perfil__ = __webpack_require__(739);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageModule", function() { return PerfilPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PerfilPageModule = (function () {
    function PerfilPageModule() {
    }
    return PerfilPageModule;
}());
PerfilPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */]
        ]
    })
], PerfilPageModule);

//# sourceMappingURL=perfil.module.js.map

/***/ }),

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_ui_auth__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_ui_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_ui_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_transfer__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_perfil_factory__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_toast__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_rootScope__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_departamentos_factory__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_message_service__ = __webpack_require__(257);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var $rootScope = __WEBPACK_IMPORTED_MODULE_9__app_rootScope__["a" /* default */];
var PerfilPage = (function () {
    function PerfilPage(navCtrl, navParams, auth, transfer, file, userFactory, toast, camera, sanitizer, departamentosFactory, messageService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.transfer = transfer;
        this.file = file;
        this.userFactory = userFactory;
        this.toast = toast;
        this.camera = camera;
        this.sanitizer = sanitizer;
        this.departamentosFactory = departamentosFactory;
        this.messageService = messageService;
        this.user = {};
        this.htmlStr = '<div class="divImagen" style="background-image:url({{ user.Avatar }});"></div>';
        this.paises = [];
        this.paisSeleccionado = {};
        this.user = $rootScope.user;
        console.log('Usuario:', this.user.Avatar);
        this.myCroppedImage = '';
        if (this.user.Identificacion == 0) {
            this.user.Identificacion = '';
        }
        if (this.user.Direccion == "null") {
            this.user.Direccion = '';
        }
        if (this.user.Telefono == 0) {
            this.user.Telefono = '';
        }
        if (this.user.TelefonoWhatsApp == 0) {
            this.user.TelefonoWhatsApp = '';
        }
        if (this.user.UrlIntroduccion == "null") {
            this.user.UrlIntroduccion = '';
        }
        if (this.user.Descripcion == "null") {
            this.user.Descripcion = '';
        }
        if (this.user.NombreEmpresa == "null") {
            this.user.NombreEmpresa = '';
        }
        if (this.user.Nit == "null") {
            this.user.Nit = '';
        }
        this.CargarPaises();
    }
    PerfilPage.prototype.GuardarAvatar = function () {
        //let usr = this.auth.getPayload().sub;
        var _this = this;
        this.userFactory
            .GuardarAvatar(this.user, this.user.Avatar)
            .then(function (result) {
            console.log(result);
            //this.user.Avatar = this.userFactory.UrlAvatar(this.user);
            //this.user.Completo = !!this.user.Identificacion;
            console.log('resultAvatar', result.Avatar);
            _this.user.Avatar = result.Avatar;
            _this.toast.show('Avatar guardado correctamente.', '2500', 'bottom').subscribe(function (toast) {
                console.log(toast);
            });
        })
            .catch(function (err) {
            console.log('error');
            _this.toast.show('error.data', '2500', 'bottom');
        });
    };
    PerfilPage.prototype.Guardar = function () {
        var _this = this;
        if (this.user.UrlIntroduccion.lastIndexOf("embed") != 24) {
            var videoId = this.user.UrlIntroduccion.split('https://youtu.be/').join('');
            console.log("videoId:", videoId);
            var rel = 'rel=0';
            var embed = 'https://www.youtube.com/embed';
            this.user.UrlIntroduccion = [embed, videoId].join('/');
            this.user.UrlIntroduccion = [this.user.UrlIntroduccion, rel].join('?');
            console.log("Url Transformada: ", this.user.UrlIntroduccion);
        }
        console.log("seleccionado para guardar:", this.paisSeleccionado);
        this.user.PaiId = this.paisSeleccionado;
        this.userFactory
            .Guardar(this.user.Id, this.user)
            .subscribe(function (result) {
            _this.user = result;
            _this.user.Telefono = _this.user.Telefono * 1;
            _this.user.Identificacion = _this.user.Identificacion * 1;
            _this.user.TelefonoWhatsApp = _this.user.TelefonoWhatsApp * 1;
            _this.user.Completo = !!_this.user.Nombre &&
                !!_this.user.Identificacion &&
                !!_this.user.Telefono &&
                !!_this.user.TelefonoWhatsApp &&
                !!_this.user.Direccion;
            var paisSeleccionadoObjeto = [].find.call(_this.paises, function (pa) {
                return pa.Id == _this.paisSeleccionado;
            });
            console.log(paisSeleccionadoObjeto.Id);
            //console.log(this.user.Pai.Nombre);
            $rootScope.user = _this.user;
            $rootScope.user.Pai = paisSeleccionadoObjeto;
            _this.messageService.sendMessage('Message from Home Component to App Component!');
            //$rootScope.paisSeleccionadoId = this.paisSeleccionado;
            _this.navCtrl.pop();
        }, function (err) { return console.log("error", err); });
    };
    PerfilPage.prototype.cancelar = function () {
        this.navCtrl.pop();
    };
    PerfilPage.prototype.CambiarAvatar = function () {
        var _this = this;
        this.camera.getPicture({
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            quality: 75,
            saveToPhotoAlbum: false,
            targetWidth: 800,
            targetHeight: 800
        })
            .then(function (image) {
            _this.user.Avatar = image;
        })
            .catch(function (error) { return console.error(error); });
    };
    ;
    PerfilPage.prototype.CargarPaises = function () {
        var _this = this;
        this.departamentosFactory
            .CargarPaises()
            .subscribe(function (result) {
            console.log("result" + result.rows);
            _this.paises = result.rows;
            _this.paisSeleccionado = $rootScope.user.Pai.Id;
            //console.log("seleccionado:"+this.paisSeleccionado);    
        }, function (err) { return console.log("error", err); });
    };
    PerfilPage.prototype.CambiarPais = function () {
        console.log(this.paisSeleccionado);
    };
    return PerfilPage;
}());
PerfilPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-perfil',template:/*ion-inline-start:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/perfil/perfil.html"*/'<ion-header>\n  <ion-navbar color="prevenir">\n    <ion-title>Perfil</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <ion-item text-center>\n    <img [src]="user.Avatar" class="divImagen"> \n    <ion-fab >\n      <button ion-fab mini (click)="CambiarAvatar()">\n        <ion-icon name="cloud-upload"></ion-icon>\n      </button>\n    </ion-fab>\n  </ion-item>\n  \n  <button ion-button round full  type="submit" class="button button-light button-full button-main" (click)="GuardarAvatar()">\n    <ion-icon name="archive"></ion-icon>\n    &nbsp;&nbsp;Guardar Avatar\n  </button>\n  \n  <h1 class="prevenir-title" text-center>Por favor completa tu perfil de usuario</h1>\n  <form>\n    <ion-list>\n    <ion-item>\n    <ion-label>\n          <ion-icon name="jet"></ion-icon>\n        </ion-label>\n       <ion-select [(ngModel)]="paisSeleccionado" [ngModelOptions]="{standalone: true}" (ngModelChange)="CambiarPais()">\n        <ion-option disabled value selected >Selecciona una</ion-option>\n        <ion-option *ngFor="let p of paises" value="{{ p.Id }}">{{ p.Nombre }}</ion-option>\n      </ion-select>\n    </ion-item>\n      <ion-item>\n        <ion-label>\n          <ion-icon name="mail"></ion-icon>\n        </ion-label>\n        <ion-input type="email" placeholder="Ingrese su correo" [(ngModel)]="user.Correo" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>\n          <ion-icon name="person"></ion-icon>\n        </ion-label>\n        <ion-input type="text" placeholder="Ingrese su nombre" [(ngModel)]="user.Nombre" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>\n          <ion-icon name="card"></ion-icon>\n        </ion-label>\n        <ion-input id="id" type="number" placeholder="Ingrese su identificación" [(ngModel)]="user.Identificacion" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>\n          <ion-icon name="locate"></ion-icon>\n        </ion-label> \n        <ion-input id="id" type="text" placeholder="Ingrese su dirección" [(ngModel)]="user.Direccion" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>\n          <ion-icon name="call"></ion-icon>\n        </ion-label>\n        <ion-input  type="number"  placeholder="Ingrese su teléfono" [(ngModel)]="user.Telefono" [ngModelOptions]="{standalone: true}">\n      </ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label>\n          <ion-icon name="logo-whatsapp"></ion-icon>\n        </ion-label>\n        <ion-input id="telwhats" type="number" step="0" placeholder="Ingrese su WhatsApp" [(ngModel)]="user.TelefonoWhatsApp" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      <ion-item *ngIf="user.Proveedor==true">\n        <ion-label>\n          <ion-icon name="videocam"></ion-icon>\n        </ion-label>\n        <ion-input id="url" type="text"  placeholder="Pegue el link de Youtube aqui" [(ngModel)]="user.UrlIntroduccion" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      <ion-item *ngIf="user.Proveedor==true">\n        <ion-textarea rows="5" placeholder="Ingrese su descripción" [(ngModel)]="user.Descripcion" [ngModelOptions]="{standalone: true}"></ion-textarea>\n      </ion-item>\n\n\n      <ion-item *ngIf="user.Proveedor==true">\n        <ion-label>\n          <ion-icon name="person"></ion-icon>\n        </ion-label>\n        <ion-input id="text" type="text"  placeholder="Ingrese el nombre de su Empresa" [(ngModel)]="user.NombreEmpresa" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      <ion-item *ngIf="user.Proveedor==true">\n        <ion-label>\n          <ion-icon name="person"></ion-icon>\n        </ion-label>\n        <ion-input  type="text"  placeholder="Ingrese el NIT de su Empresa" [(ngModel)]="user.Nit" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      <ion-item *ngIf="user.Proveedor==true">\n        <ion-label>\n          <ion-icon name="person"></ion-icon>\n        </ion-label>\n        <ion-input  type="text"  placeholder="Ingrese la página Web" [(ngModel)]="user.UrlPagina" [ngModelOptions]="{standalone: true}">\n        </ion-input>\n      </ion-item>\n      \n        <button class="buttonprofile" type="submit" ion-button round full color="danger" (click)="Guardar()" >\n          <ion-icon name="archive"></ion-icon>&nbsp;Guardar\n        </button> \n      \n      \n        <button class="buttonprofilesec" ion-button round full color="energized" (click)="cancelar()" >\n          <ion-icon name="arrow-back"></ion-icon>&nbsp;Cancelar\n        </button> \n      \n    </ion-list>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/perfil/perfil.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ng2_ui_auth__["AuthService"],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_transfer__["a" /* Transfer */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_5__providers_perfil_factory__["a" /* UserFactory */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_toast__["a" /* Toast */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__["e" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_10__providers_departamentos_factory__["a" /* DepartamentosFactory */],
        __WEBPACK_IMPORTED_MODULE_11__providers_message_service__["a" /* MessageService */]])
], PerfilPage);

//# sourceMappingURL=perfil.js.map

/***/ })

});
//# sourceMappingURL=10.main.js.map