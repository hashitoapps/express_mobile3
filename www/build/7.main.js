webpackJsonp([7],{

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__results__ = __webpack_require__(741);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultsModule", function() { return ResultsModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ResultsModule = (function () {
    function ResultsModule() {
    }
    return ResultsModule;
}());
ResultsModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__results__["a" /* Results */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__results__["a" /* Results */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__results__["a" /* Results */]
        ]
    })
], ResultsModule);

//# sourceMappingURL=results.module.js.map

/***/ }),

/***/ 741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_productos_factory__ = __webpack_require__(232);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Results; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*import { NgModule }                   from '@angular/core';
import { FormsModule }                from '@angular/forms';*/
/**
 * Generated class for the ResultsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Results = (function () {
    function Results(navCtrl, navParams, loadingCtrl, productosFactory) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.productosFactory = productosFactory;
        this.page = 0;
        this.pageSize = 10;
        this.moredata = true;
        this.resultados = 0;
        this.publicaciones = [];
        //this.c = null;
        this.cargarSiguientePagina(null);
    }
    Results.prototype.ionViewDidLoad = function () {
        this.CrearFiltro();
    };
    Results.prototype.cargarPagina = function () {
        return this.productosFactory
            .LoadPublicationList(this.page, this.pageSize, 'createdAt', 'DESC', //c
        this.CrearFiltro());
    };
    Results.prototype.cargarPublicacion = function (Id) {
        this.navCtrl.push('PublicacionPage', { Id: Id });
    };
    Results.prototype.cargarSiguientePagina = function (infiniteScroll) {
        var _this = this;
        this.page += 1;
        this.cargarPagina()
            .subscribe(function (result) {
            var pubs = result.rows;
            console.log(pubs);
            _this.moredata = pubs.length == _this.pageSize;
            _this.resultados = result.count;
            _this.publicaciones = _this.publicaciones.concat(pubs);
            if (!!infiniteScroll) {
                infiniteScroll.complete();
            }
            console.log(result.count);
        }, function (err) {
            console.log(err);
        });
    };
    Results.prototype.RefrescarInformacion = function (ionRefresher) {
        var _this = this;
        this.page = 1;
        this.publicaciones = [];
        this.cargarPagina()
            .subscribe(function (result) {
            var pubs = result.rows;
            _this.moredata = pubs.length == _this.pageSize;
            _this.resultados = result.count;
            _this.publicaciones = _this.publicaciones.concat(pubs);
            ionRefresher.complete();
        }, function (err) {
            console.log(err);
        });
    };
    Results.prototype.CrearFiltro = function () {
        var filter = [];
        console.log("crearfiltro", this.navParams.get('IdCategoria'));
        filter.push('IdCategoria=' + this.navParams.get('IdCategoria'));
        filter.push('IdSubCategoria=' + this.navParams.get('IdSubCategoria'));
        filter.push('IdVariedad=' + this.navParams.get('IdVariedad'));
        filter.push('Descripcion=' + this.navParams.get('Descripcion')) || '';
        filter.push('IdMunicipio=' + this.navParams.get('IdMunicipio'));
        return filter.join('&');
    };
    return Results;
}());
Results = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-results',template:/*ion-inline-start:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/results/results.html"*/'<ion-header>\n\n  <ion-navbar color="prevenir">\n    <ion-title>Grupo Prevenir Express</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-refresher (ionRefresh)="RefrescarInformacion($event)">\n    <ion-refresher-content\n    pulling-text="Arrastra para actualizar...">  \n  </ion-refresher-content>\n</ion-refresher>\n\n\n<h1 text-center class="prevenir-title">Resultados de la búsqueda</h1>\n<h5 text-center class="prevenir-title">Ofertas encontradas {{ resultados | number:0 }}</h5>\n\n<ion-list>\n  <ion-card *ngFor="let pubs of publicaciones">\n    <a  (click)="cargarPublicacion({Id: pubs.Id})">\n    <ion-slides>\n      <ion-slide *ngFor="let img of pubs.Imagens">\n        <div class="box blue">\n          <img [src]="img.RutaGrande" alt="">\n        </div>          \n      </ion-slide>\n    </ion-slides>\n    <ion-card-content>\n      <!--<div>-->\n      <h2>{{ pubs.Nombre }}</h2>\n      <h3  *ngIf="pubs.PorcentajePromo!=0"><ion-icon name="thumbs-up"></ion-icon>Descuento <small>&nbsp;{{ pubs.PorcentajePromo }}&nbsp;%</small></h3>\n      <h3 *ngIf="pubs.PorcentajePromo==0"><ion-icon name="thumbs-down"></ion-icon>Particulares <small>&nbsp;{{ pubs.Precio | number:0 }}&nbsp;</small></h3>\n      <h3  *ngIf="pubs.PorcentajePromo==0"><ion-icon name="thumbs-up"></ion-icon>Prevenir Express {{ pubs.PrecioDesc | number:0 }}</h3>\n      <p >{{ pubs.DescripcionCorta }}</p><br>\n      <!-- </div>-->\n    </ion-card-content>\n  </a>\n  </ion-card>\n</ion-list>\n<ion-infinite-scroll (ionInfinite)="cargarSiguientePagina($event)" *ngIf="moredata">\n <ion-infinite-scroll-content></ion-infinite-scroll-content>\n</ion-infinite-scroll>\n</ion-content>'/*ion-inline-end:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/results/results.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_productos_factory__["a" /* ProductosFactory */]])
], Results);

//# sourceMappingURL=results.js.map

/***/ })

});
//# sourceMappingURL=7.main.js.map