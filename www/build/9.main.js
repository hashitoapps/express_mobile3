webpackJsonp([9],{

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__publicacion__ = __webpack_require__(730);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicacionPageModule", function() { return PublicacionPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var PublicacionPageModule = (function () {
    function PublicacionPageModule() {
    }
    return PublicacionPageModule;
}());
PublicacionPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__publicacion__["a" /* SafePipe */],
            __WEBPACK_IMPORTED_MODULE_2__publicacion__["b" /* PublicacionPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__publicacion__["b" /* PublicacionPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__publicacion__["b" /* PublicacionPage */]
        ]
    })
], PublicacionPageModule);

//# sourceMappingURL=publicacion.module.js.map

/***/ }),

/***/ 730:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_ui_auth__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_ui_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_ui_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_productos_factory__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_toast__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SafePipe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PublicacionPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SafePipe = (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    };
    return SafePipe;
}());
SafePipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'safe'
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["e" /* DomSanitizer */]])
], SafePipe);

/**
 * Generated class for the PublicacionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PublicacionPage = (function () {
    function PublicacionPage(navCtrl, navParams, productosFactory, loadingCtrl, auth, toast, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.productosFactory = productosFactory;
        this.loadingCtrl = loadingCtrl;
        this.auth = auth;
        this.toast = toast;
        this.alertCtrl = alertCtrl;
        this.publicacion = { Usuario: {} };
        this.mensaje = '';
        this.proveedor = {};
        this.usr = this.auth.getPayload().sub;
        this.videito = '';
        this.frame = '';
        this.loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: "Espera un momento<br>estamos cargando la publicación... ",
            duration: 3000
        });
        this.loading.present();
        console.log("Entro aquiiiii", this.navParams.get('Id'));
        this.productosFactory
            .LoadPublication(this.navParams.get('Id').Id)
            .subscribe(function (result) {
            _this.publicacion = result;
            _this.videito = _this.publicacion.Usuario.UrlIntroduccion;
            console.log(result);
            _this.loading.dismiss();
        }, function (error) {
            _this.loading.dismiss();
            console.log(error);
        });
        this.frame = '';
        console.log(this.publicacion);
        console.log(this.videito);
        //console.log(this.frame);
    }
    PublicacionPage.prototype.EnviarMensaje = function () {
        var _this = this;
        var contactForm = {
            Name: '',
            Email: '',
            Message: ''
        };
        contactForm.Name = this.usr.Nombre;
        contactForm.Email = this.usr.Correo;
        contactForm.Message = this.mensaje;
        this.productosFactory
            .SendContactMail(this.publicacion.Id, contactForm)
            .subscribe(function (result) {
            _this.toast.show('Mensaje enviado correctamente.', '2500', 'bottom');
        }, function (error) {
            console.log(error);
        });
        this.loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: "Espera un momento<br>estamos enviando tu mensaje... ",
            duration: 3000
        });
        this.loading.present();
        this.timeout();
    };
    ;
    PublicacionPage.prototype.AgendarCita = function (Id, usuario) {
        console.log("Datos publicacion", this.publicacion);
        this.navCtrl.push('CitasPage', { Id: this.publicacion.Id, usuario: this.publicacion.Usuario });
    };
    PublicacionPage.prototype.timeout = function () {
        var _this = this;
        setTimeout(function () {
            _this.mensaje = '';
            _this.loading.dismiss();
        }, 3000);
    };
    PublicacionPage.prototype.MostrarHistorial = function () {
        var alert = this.alertCtrl.create({
            title: 'Click',
            buttons: ['Dismiss']
        });
        alert.present();
        //alert("Click");
    };
    PublicacionPage.prototype.IrAPublicacionesDelUsuario = function () {
        this.navCtrl.push('PublicacionesUsuarioPage', { Id: this.publicacion.Usuario.Id, Nombre: this.publicacion.Usuario.Nombre,
            NombreEmpresa: this.publicacion.Usuario.NombreEmpresa, Nit: this.publicacion.Usuario.Nit, Proveedor: this.publicacion.Usuario.Proveedor });
    };
    ;
    return PublicacionPage;
}());
PublicacionPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-publicacion',template:/*ion-inline-start:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/publicacion/publicacion.html"*/'<ion-header>\n\n  <ion-navbar color="prevenir">\n    <ion-title>{{ publicacion.Nombre }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-card>\n      <ion-item>\n        <ion-avatar item-start>\n          <img src="{{ publicacion.Usuario.Avatar }}" (click)="MostrarHistorial()">\n        </ion-avatar>\n        <div *ngIf="publicacion.Usuario.Proveedor==true"><h2>{{ publicacion.Usuario.NombreEmpresa }}</h2></div>\n        <div *ngIf="publicacion.Usuario.Proveedor==false"><h2>{{ publicacion.Usuario.Nombre }}</h2></div>\n        <div *ngIf="publicacion.Usuario.Proveedor==true"><h2>NIT {{ publicacion.Usuario.Nit }}</h2></div>\n        <small>{{ publicacion.Usuario.Descripcion }}</small>\n\n      </ion-item>\n      <button ion-button round full color="danger" (click)="IrAPublicacionesDelUsuario({Id: Id})">\n        Mas publicaciones\n      </button>\n    </ion-card>\n\n    <ion-card>\n      <ion-slides>\n        <ion-slide *ngFor="let img of publicacion.Imagens">\n          <div class="box blue">\n          <img [src]="img.RutaGrande" alt="Imagen">\n          </div>\n        </ion-slide>\n      </ion-slides>\n\n      <ion-card-content>\n\n        <h2>{{ publicacion.Nombre }}</h2>\n        <h2  *ngIf="publicacion.PorcentajePromo!=0"><ion-icon name="thumbs-up"></ion-icon>Descuento <small>&nbsp;{{ publicacion.PorcentajePromo }}&nbsp;%</small></h2>\n        <h2  *ngIf="publicacion.PorcentajePromo==0"><ion-icon name="thumbs-down"></ion-icon>Particulares <small>&nbsp;{{ publicacion.Precio | number:0 }}&nbsp;</small></h2>\n        <h2  *ngIf="publicacion.PorcentajePromo==0"><ion-icon name="thumbs-up"></ion-icon>Prevenir Express {{ publicacion.PrecioDesc | number:0 }}</h2>\n        <br><p>{{ publicacion.DescripcionCorta }}</p><br>\n        <p>{{ publicacion.DescripcionLarga }}</p>\n       </ion-card-content>\n    </ion-card>\n    <ion-card>\n      <ion-card-header>\n        <ion-icon name="videocam"></ion-icon>\n        Conocenos mejor\n      </ion-card-header>\n      <ion-card-content>\n        <div class="embed-container">\n        <iframe width="560" height="315" [src]="videito | safe" frameborder="0" rel="0"></iframe>\n        </div>\n      </ion-card-content>\n    </ion-card>\n    <ion-card>\n      <ion-card-header>\n        <ion-icon name="mail"></ion-icon>\n        Ponte en contacto\n      </ion-card-header>\n      <ion-item>\n      <ion-avatar item-start>\n          <img src="{{ publicacion.Usuario.Avatar }}" (click)="MostrarHistorial()">\n      </ion-avatar>\n          <h2>{{ publicacion.Usuario.Nombre }}</h2>\n           <p>{{ publicacion.Usuario.Descripcion }}</p>\n        </ion-item>\n      <ion-card-content>\n        <ion-item>\n          <ion-input type="text" placeholder="Nombre" [(ngModel)]="usr.Nombre" [ngModelOptions]="{standalone: true}" readonly>\n          </ion-input>\n       </ion-item>\n        <ion-item>\n           <ion-input type="email" placeholder="Correo" [(ngModel)]="usr.Correo" [ngModelOptions]="{standalone: true}" readonly>\n          </ion-input>\n       </ion-item>\n       <ion-item>\n        <ion-textarea rows="5" placeholder="Mensaje" [(ngModel)]="mensaje" [ngModelOptions]="{standalone: true}"></ion-textarea>\n      </ion-item>\n       <button ion-button round full color="danger" (click)="EnviarMensaje()"\n        [disabled]="mensaje.length < 8">\n        Enviar mensaje\n        </button>\n         <button ion-button round full  type="submit"  (click)="AgendarCita()">\n           <ion-icon name="archive"></ion-icon>\n           &nbsp;&nbsp;Agendar cita\n         </button>\n      </ion-card-content>\n    </ion-card>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/publicacion/publicacion.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_productos_factory__["a" /* ProductosFactory */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ng2_ui_auth__["AuthService"],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_toast__["a" /* Toast */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* AlertController */]])
], PublicacionPage);

//# sourceMappingURL=publicacion.js.map

/***/ })

});
//# sourceMappingURL=9.main.js.map