webpackJsonp([11],{

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__misreservas__ = __webpack_require__(737);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MisreservasPageModule", function() { return MisreservasPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MisreservasPageModule = (function () {
    function MisreservasPageModule() {
    }
    return MisreservasPageModule;
}());
MisreservasPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__misreservas__["a" /* MisreservasPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__misreservas__["a" /* MisreservasPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__misreservas__["a" /* MisreservasPage */]
        ]
    })
], MisreservasPageModule);

//# sourceMappingURL=misreservas.module.js.map

/***/ }),

/***/ 737:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_perfil_factory__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__ = __webpack_require__(230);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MisreservasPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the MisreservasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MisreservasPage = (function () {
    function MisreservasPage(navCtrl, navParams, toast, userFactory) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.userFactory = userFactory;
        this.banners = [];
        this.miListaReservas = [];
        console.log("entro aqui en citas", this.navParams.get('usuario'));
        this.userFactory
            .CargarBanners()
            .subscribe(function (res) {
            _this.banners = res.rows;
            console.log('banners: ', _this.banners);
        }, function (err) {
            console.log('error: ', err);
        });
        this.ConsultarReservas();
    }
    MisreservasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MisreservasPage');
    };
    MisreservasPage.prototype.ConsultarReservas = function () {
        var _this = this;
        this.userFactory
            .CargarCitasReservadas(this.navParams.get('usuario'))
            .subscribe(function (result) {
            _this.miListaReservas = result;
            console.log("Mi lista Reserca", _this.miListaReservas);
        }, function (err) {
            console.log('error on local data saved');
        });
    };
    MisreservasPage.prototype.Eliminar = function (Id) {
        var _this = this;
        console.log('Identificacdor eliminar', Id);
        this.userFactory
            .EliminarCita(Id)
            .subscribe(function (result) {
            //this.miListaReservas = result;
            _this.ConsultarReservas();
            console.log("Mi lista Reserca", _this.miListaReservas);
            _this.toast.show('La reserva de si cita fue guardada exitosamente.', '2500', 'bottom').subscribe(function (toast) {
                console.log(toast);
            });
        }, function (err) {
            console.log('error on local data saved');
        });
    };
    return MisreservasPage;
}());
MisreservasPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-misreservas',template:/*ion-inline-start:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/misreservas/misreservas.html"*/'<!--\n  Generated template for the MisreservasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="prevenir">\n    <ion-title>Mis reservas</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <ion-slides>\n    <ion-slide *ngFor="let img of banners">\n      <div class="box blue">\n        <img [src]="img.Ruta" alt="Imagen">\n      </div>\n    </ion-slide>\n  </ion-slides>\n  <div class="spacer" style="height: 20px;"></div>\n  <div><h2 class="prevenir-title" text-center>Historial de reservas de citas</h2></div>\n  <div class="spacer" style="height: 15px;"></div>\n  <ion-list>\n   <ion-option *ngFor="let dir of miListaReservas">\n    <ion-card>\n     <ion-card-content >\n       <ion-item>\n         <ion-label>\n           <ion-icon name="alarm"></ion-icon>\n         </ion-label>\n         <ion-input type="text" [(ngModel)]="dir.hora" [ngModelOptions]="{standalone: true}" >\n         </ion-input>\n       </ion-item>\n       <ion-item>\n         <ion-label>\n           <ion-icon name="calendar"></ion-icon>\n         </ion-label>\n         <ion-input type="text" [(ngModel)]="dir.fecha_reserva" [ngModelOptions]="{standalone: true}" >\n         </ion-input>\n       </ion-item>\n       <ion-item>\n         <ion-label>\n           <ion-icon name="person"></ion-icon>\n         </ion-label>\n         <ion-input type="text" [(ngModel)]="dir.empresa" [ngModelOptions]="{standalone: true}" >\n         </ion-input>\n       </ion-item>\n       <ion-item>\n         <ion-label>\n           <ion-icon name="call"></ion-icon>\n         </ion-label>\n         <ion-input type="text" [(ngModel)]="dir.telefono" [ngModelOptions]="{standalone: true}" >\n         </ion-input>\n       </ion-item>\n       <ion-item>\n         <ion-label>\n           <ion-icon name="list"></ion-icon>\n         </ion-label>\n         <ion-input type="text" [(ngModel)]="dir.ntipopago" [ngModelOptions]="{standalone: true}" >\n         </ion-input>\n       </ion-item>\n       <ion-item>\n         <ion-label>\n           <ion-icon name="list"></ion-icon>\n         </ion-label>\n         <ion-input type="text" [(ngModel)]="dir.estado_pago" [ngModelOptions]="{standalone: true}" >\n         </ion-input>\n       </ion-item>\n        <div class="spacer" style="height: 20px;"></div>\n       <button ion-button round full  color="danger"  (click)="Eliminar(dir.id)">\n         <ion-icon name="archive"></ion-icon>\n         &nbsp;&nbsp;Eliminar reserva\n       </button>\n     </ion-card-content >\n    </ion-card>\n   </ion-option>\n</ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/alejandro/Documents/proyecto-prevenir/codigo24dictemp/express_mobile3/src/pages/misreservas/misreservas.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_toast__["a" /* Toast */],
        __WEBPACK_IMPORTED_MODULE_2__providers_perfil_factory__["a" /* UserFactory */]])
], MisreservasPage);

//# sourceMappingURL=misreservas.js.map

/***/ })

});
//# sourceMappingURL=11.main.js.map