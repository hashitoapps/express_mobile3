import { Injectable }           from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
import 'rxjs/add/operator/map';
import { Transfer, 
         FileUploadOptions, 
         TransferObject } 		  from '@ionic-native/transfer';
import { File } 				        from '@ionic-native/file';


/*
  Generated class for the ProductosFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
    */
  @Injectable()
  export class ProductosFactory {

    public urlBase: string = Config.ApiUrl;
    public fileTransfer: TransferObject;



    constructor(public http: JwtHttp, private transfer: Transfer, private file: File) {
      console.log('Hello ProductosFactory Provider');
      this.fileTransfer = this.transfer.create();
    }

    LoadLastPublications () {
      let url = [
      'productos',
      [
      'page=1',
      'pageSize=4',
      'orderBy=createdAt',
      'sortOrder=DESC',
      ].join('&')
      ].join('?');

      return this.http.get([this.urlBase, url].join('/')).map(res => res.json());
    }; 

    LoadMostPopulars () {
      let url = [
      'productos',
      [
      'page=1',
      'pageSize=6',
      'orderBy=MeGusta',
      'sortOrder=DESC',
      ].join('&')
      ].join('?');

      return this.http.get([this.urlBase, url].join('/')).map(res => res.json());
    };

    LoadMostViewed () {
      let url = [
      'productos',
      [
      'page=1',
      'pageSize=6',
      'orderBy=Visitas',
      'sortOrder=DESC',
      ].join('&')
      ].join('?');

      return this.http.get([this.urlBase, url].join('/')).map(res => res.json());
    };

    LoadPublicationList (page, pageSize, orderBy, sortOrder, filter) {
      let url = [
      'productos/filter',
      [
      ['page', page].join('='),
      ['pageSize', pageSize].join('='),
      ['orderBy', orderBy].join('='),
      ['sortOrder', sortOrder].join('='),
      filter
      ].join('&')
      ].join('?');

      return this.http.get([this.urlBase, url].join('/')).map(res => res.json());
    };

    LoadPublicationListByUser (UsuarioId, page, pageSize, orderBy, sortOrder, filter) {
      let url = [
      'productos',
      [
      ['page', page].join('='),
      ['pageSize', pageSize].join('='),
      ['orderBy', orderBy].join('='),
      ['sortOrder', sortOrder].join('='),
      filter
      ].join('&')
      ].join('?');

      return this.http.get([this.urlBase, 'usuarios', UsuarioId, url].join('/')).map(res => res.json());
    };

    LoadSimilarPublications (Id) {

      return this.http.get([this.urlBase, 'productos', Id, 'similares'].join('/')).map(res => res.json());
    };

    LoadPublication (Id) {

      return this.http.get([this.urlBase, 'productos', Id, 'visitar'].join('/')).map(res => res.json());
    };

    EliminarPublicacion (Id) {

      return this.http.delete([this.urlBase, 'productos', Id].join('/'));
    };

    ActualizarPublicacion (Id, publicacion) {

      return this.http.put([this.urlBase, 'productos' ,Id].join('/'), publicacion).map(res => res.json());
    };

    GuardarPublicacion (publicacion) {
      return this.http.post([this.urlBase, 'productos'].join('/'), publicacion).map(res => res.json());
    };

    GuardarImagen (publicacion, filePath) {
      console.log('filepath:', filePath);
      return this.http.post([
        this.urlBase,
        'productos',
        publicacion.Id,
        'imagen-base64'
      ].join('/'), {imagen: filePath}).map(res => res.json());   
    };
      // let options: FileUploadOptions = {
      //   fileKey: 'file',
      //   fileName: "image.png",
      //   httpMethod: 'POST',
      //   mimeType: 'image/png',
      //   chunkedMode: false
      // };

      // console.log('Image:', filePath);
      // console.log('Image URL:', [
      //     this.urlBase,
      //     'productos',
      //     publicacion.Id,
      //     'imagen'
      //   ].join('/'));

      
      // return this.fileTransfer.upload(
      //   filePath, [
      //     this.urlBase,
      //     'productos',
      //     publicacion.Id,
      //     'imagen'
      //   ].join('/'), options);
  

    // upload (publicacion) {
    //   console.log("pub:",publicacion);
    //   let options: FileUploadOptions = {
    //     fileKey: 'file',
    //     fileName: 'avatar.jpg',
    //     httpMethod: 'POST',
    //     mimeType: 'image/png',
    //     params: publicacion
        
    //   }
    // }
         // let req = {
     //    method: 'POST',
     //    url: [this.urlBase,'productos' ,publicacion.Id,'imagen'].join('/'),
     //    headers: {
     //      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
     //    },
     //    data:filePath
     //  };

     //  this.http(req).success(function(data, status, headers, config) {
     //    console.log("COSITS:"+data.message);
     //    if(data.message=="ok"){
     //      $state.go("home");  

     //    }
     //  })
     //  .error(function(data, status, headers, config) {


     //  });

    SendContactMail (Id, contactInfo) {
      return this.http.post([
        this.urlBase,
        'productos',
        Id,
        'correo-contacto'
        ].join('/'), contactInfo);
    };

    GuardarProductoMunicipio(param) {
      return this.http.post([
        this.urlBase,
        'productomunicipio'
        ].join('/'), param
      ).map(res => res.json());
    }

    EliminarProductoMunicipioTodos(Id){
      return this.http.delete([
        this.urlBase,
        'productomunicipio',
        Id,
        'deleteall'
        ].join('/')).map(res => res.json());
    }

  }
