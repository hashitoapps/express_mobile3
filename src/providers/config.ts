import { Injectable } from '@angular/core';

@Injectable()
export class Config {
  public static ApiUrl: string = 'http://api.prevenirexpress.com/api/v00';
  //public static ApiUrlCitas: string = 'http://localhost:8080/express-citas/site/services';
  //public static ApiUrlCitasUsuario: string = 'http://localhost:8080/express-citas/site/servicio';

  public static ApiUrlCitas: string = 'http://citas.prevenirexpress.com/services';
  public static ApiUrlCitasUsuario: string = 'http://citas.prevenirexpress.com/servicio';

  constructor() {
    console.log('Hello Config Provider');
  }
}
