import { Injectable }           from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
import 'rxjs/add/operator/map';

@Injectable()
export class SearchFactory {
  urlBase: string = Config.ApiUrl;

  constructor(public http: JwtHttp) {
    console.log('Hello SearchFactory Provider');
  }

  LoadCategories() {
    return this.http.get([this.urlBase, 'subcategorias'].join('/')).map(res => res.json());
  };

  LoadDepartamentos() {
    return this.http.get([this.urlBase, 'departamentos'].join('/')).map(res => res.json());
  };
}
