import { Injectable }           from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
import 'rxjs/add/operator/map';

/*
  Generated class for the DepartamentosFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DepartamentosFactory {
  urlBase: string = Config.ApiUrl;

  constructor(public http: JwtHttp) {
    console.log('Hello DepartamentosFactory Provider');
  }

  CargarDepartamentos(id) {
    return this.http
      .get([this.urlBase, 'departamentos',id].join('/'))
      .map(res => res.json());
  }

  CargarPaises() {
    return this.http
      .get([this.urlBase, 'paises'].join('/'))
      .map(res => res.json());

  }


}
