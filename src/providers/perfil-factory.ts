import { Injectable } 			  		from '@angular/core';
import { JwtHttp }                from 'ng2-ui-auth';
import { Config }                 from './config';
import 'rxjs/add/operator/map';
import { Transfer,
	       FileUploadOptions,
	       TransferObject } 		    from '@ionic-native/transfer';
import { File } 				  				from '@ionic-native/file';

/*
  Generated class for the PerfilFactoryProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
  	*/
  @Injectable()
  export class UserFactory {

  	public urlBase: string = Config.ApiUrl;
		public urlBaseCitas: string = Config.ApiUrlCitas;
		public ApiUrlCitasUsuario: string = Config.ApiUrlCitasUsuario;
  	public fileTransfer: TransferObject;

  	constructor(public http: JwtHttp, private transfer: Transfer, private file: File) {
  		console.log('Hello UserFactory Provider');
  		this.fileTransfer = this.transfer.create();
  	}

  	Guardar (Id, usr) {
  		return this.http.put([this.urlBase, 'usuarios', Id].join('/'), usr).map(res => res.json());
  	};

  	GuardarAvatar (usr, filePath) {
      console.log('AvatarfilePath', filePath);

      let options: FileUploadOptions = {
  			fileKey: 'file',
  			fileName: 'avatar.png',
  			httpMethod: 'POST',
  			mimeType: 'image/png',
        params: usr/*,
      chunkedMode: '',
      headers: '',*/
    };

    console.log(this.fileTransfer);
    console.log([
        this.urlBase,
        'usuarios',
        usr.Id,
        'nuevoavatar'
      ].join('/'));

    return this.fileTransfer.upload(
    	filePath, [
        this.urlBase,
        'usuarios',
        usr.Id,
        'nuevoavatar'
      ].join('/'), options);
  };

  CargarBanners () {
    return this.http.get([this.urlBase, 'ImagenesBanner' ].join('/')).map(res => res.json());
  }

  CargarUsuario (Id) {
  	return this.http.get([this.urlBase, 'usuarios', Id].join('/')).map(res => res.json());
  };

  UrlAvatar (user) {
  	return [this.urlBase.replace('/api/v00',''), 'avatars', user.Id + '.png'].join('/');
  };

  GoToFacebook (access_token){
  	return this.http.post([this.urlBase, 'usuarios','facebook'].join('/'), access_token).map(res => res.json());
  };

	//citas
	CargarCitas (empresa,fecha,dia) {
    return this.http.get([this.urlBaseCitas, 'citas',empresa,fecha,dia ].join('/')).map(res => res.json());
  }

	//cargar una cita
	CargarUnaCIta (empresa,dia,hora) {
    return this.http.get([this.urlBaseCitas, 'unhorario',empresa,dia,hora ].join('/')).map(res => res.json());
  }

	//cargar una cita
	CargarCitasReservadas (usuario) {
    return this.http.get([this.urlBaseCitas, 'listareservas',usuario ].join('/')).map(res => res.json());
  }

	// guardar citas
	GuardarCita (reserva) {
		return this.http.post([this.urlBaseCitas, 'guardarcita'].join('/'),reserva).map(res => res.json());
	};

	// guardar citas
	EliminarCita (id) {
		return this.http.delete([this.urlBaseCitas, 'cancelarcita',id].join('/')).map(res => res.json());
	};

// guardar usaurio
	GuardarUsuario (usuario) {
		return this.http.post([this.ApiUrlCitasUsuario, 'guardarUsuario'].join('/'),usuario).map(res => res.json());
		//console.log("resultado", usuario)
	};

	CargarListaServicios (empresaid) {
    return this.http.get([this.urlBaseCitas, 'listaservicios',empresaid].join('/')).map(res => res.json());
  }


}
