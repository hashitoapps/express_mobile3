import { Injectable } 					from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
import 'rxjs/add/operator/map';

/*
  Generated class for the CategoriasFactoryProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
  	*/
  @Injectable()
  export class CategoriasFactoryProvider {

  	public urlBase: string = Config.ApiUrl;

  	constructor(public http: JwtHttp) {
  		console.log('Hello CategoriasFactoryProvider Provider');
  	}


  	LoadCategories () {
  		return this.http.get([this.urlBase, 'subcategorias'].join('/'));
  	};

  	LoadDepartamentos () {
  		return this.http.get([this.urlBase, 'departamentos'].join('/'));
  	};
  }
