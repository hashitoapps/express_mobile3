import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicacionesUsuarioPage } from './publicacionesusuario';

@NgModule({
  declarations: [
    PublicacionesUsuarioPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicacionesUsuarioPage),
  ],
  exports: [
    PublicacionesUsuarioPage
  ]
})
export class PublicacionPageModule {}