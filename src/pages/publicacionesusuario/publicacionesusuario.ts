import { Component }                           from '@angular/core';
import { AuthService }                         from 'ng2-ui-auth';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductosFactory }                    from '../../providers/productos-factory';
import { Loading, 
  LoadingController }                   from 'ionic-angular';
  import { Toast }                               from '@ionic-native/toast';
  import { AlertController }                     from 'ionic-angular';

  @IonicPage()
  @Component({
    selector: 'page-publicacionesusuario',
    templateUrl: 'publicacionesusuario.html',
  })

  export class PublicacionesUsuarioPage {

    public page;
    public pageSize;
    public moredata;
    public resultados;
    public publicaciones;
    public usuario;

    constructor(public navCtrl: NavController, 
      public navParams: NavParams, 
      public productosFactory: ProductosFactory, 
      public loadingCtrl: LoadingController,
      public auth: AuthService,
      public toast: Toast,
      private alertCtrl: AlertController



      ) {

      this.page          = 0;
      this.pageSize      = 10;
      this.moredata      = true;
      this.resultados    = 0;
      this.publicaciones = [];
      this.usuario={};

      //ionicToast.show("Usuario:"+$state.params.Id, 'bottom', false, 2500);
      this.usuario.Id = this.navParams.get('Id');
      this.usuario.Nombre = this.navParams.get('Nombre');
      this.usuario.Proveedor = this.navParams.get('Proveedor');
      this.usuario.NombreEmpresa = this.navParams.get('NombreEmpresa');
      if(this.usuario.Proveedor=="true"){
        this.usuario.Nit = "NIT: "+this.navParams.get('Nit');
      }

      console.log("proveedor:"+this.navParams.get('Proveedor'));
      console.log("nempersa:"+this.navParams.get('NombreEmpresa'));
      console.log("nit:"+this.navParams.get('Nit'));


      this.cargarSiguientePagina(null);
    }

    RefrescarInformacion (ionRefresher) {
    this.page = 1;
    this.publicaciones = [];
    this.cargarPagina()
      .subscribe((result:any) => {
         let pub = result.rows;

        this.moredata      = pub.length == this.pageSize;
        this.resultados    = result.count;
        this.publicaciones = this.publicaciones.concat(pub);

        ionRefresher.complete();
      },
      err => {
        console.log(err); 
      });
  }

cargarPublicacion(Id) {
    this.navCtrl.push('PublicacionPage', {Id: Id});
  }

  cargarPagina () {

    //state tiene el Id del usuario de la publicacion

    return this.productosFactory
      .LoadPublicationListByUser(
       this.usuario.Id,
       this.page,
       this.pageSize,
        'createdAt',
        'DESC', ''
      );
  };

   cargarSiguientePagina (infiniteScroll) {
   this.page += 1;

   this.cargarPagina()
      .subscribe((result:any) => {
        let pub = result.rows;

        this.moredata      = pub.length == this.pageSize;
        this.resultados    = result.count;
        this.publicaciones = this.publicaciones.concat(pub);
        console.log("publicaciones:"+this.publicaciones);

        if (!!infiniteScroll) {
          infiniteScroll.complete();
        }
        console.log(result.count);
      },
      err => {
        console.log(err); 
      }) 
  };

  }