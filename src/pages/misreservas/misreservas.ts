import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserFactory}                          from '../../providers/perfil-factory';
import { Toast }                 from '@ionic-native/toast';

/**
 * Generated class for the MisreservasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-misreservas',
  templateUrl: 'misreservas.html',
})
export class MisreservasPage {
   public banners;
   public miListaReservas;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toast: Toast,
    public userFactory: UserFactory) {

      this.banners     = [];
      this.miListaReservas = [];
      console.log("entro aqui en citas",this.navParams.get('usuario'));
      this.userFactory
      .CargarBanners()
      .subscribe(
        res => {
          this.banners = res.rows;
          console.log('banners: ', this.banners);
        },
        err => {
          console.log('error: ', err);
    });
    this.ConsultarReservas();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MisreservasPage');
  }

  ConsultarReservas(){
      this.userFactory
      .CargarCitasReservadas(this.navParams.get('usuario'))
        .subscribe(result => {
          this.miListaReservas = result;
          console.log("Mi lista Reserca", this.miListaReservas);
        },
          err => {
            console.log('error on local data saved');
        });
  }

  Eliminar(Id){
    console.log('Identificacdor eliminar',Id);
      this.userFactory
      .EliminarCita(Id)
      .subscribe(result => {
        //this.miListaReservas = result;
        this.ConsultarReservas();
        console.log("Mi lista Reserca", this.miListaReservas);
         this.toast.show('La reserva de si cita fue guardada exitosamente.','2500','bottom').subscribe(
            toast => {
              console.log(toast);
            });
      },
        err => {
          console.log('error on local data saved');
      });
  }

}
