import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminPublicacionesPage } from './adminpublicaciones';

@NgModule({
  declarations: [
    AdminPublicacionesPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminPublicacionesPage),
  ],
  exports: [
    AdminPublicacionesPage
  ]
})
export class PublicacionPageModule {}