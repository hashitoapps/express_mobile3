import { Component }                           from '@angular/core';
import { AuthService }                         from 'ng2-ui-auth';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductosFactory }                    from '../../providers/productos-factory';
import { LoadingController }                          from 'ionic-angular';
import { Toast }                               from '@ionic-native/toast';
import { AlertController }                     from 'ionic-angular';
import { SearchFactory }                       from '../../providers/search-factory';
//import { ImagePicker }                         from '@ionic-native/image-picker';
import { ActionSheetController }               from 'ionic-angular';
import {  ModalController, ViewController}     from 'ionic-angular';
import { DepartamentosFactory }                from '../../providers/departamentos-factory';
import { Observable } from 'rxjs/Observable';
import 'rxjs';
import * as rootScope            from '../../app/rootScope';


const $rootScope = rootScope.default;

@IonicPage()
@Component({
  selector: 'page-adminpublicaciones',
  templateUrl: 'listado.html',
})
export class AdminPublicacionesPage {

  public page;
  public pageSize;
  public moredata;
  public resultados;
  public publicaciones;
  public categorias;
  public subcategorias;
  public imagenes;
  public ciudadesSeleccionadas;
  public publicacion;
  public municipioId;
  public Departamentos;
  public departamentoId = null; 
  public variedad;
  public subcategoria;
  public Municipios;
  public terminos: Boolean;
  public modalNvaPublicacion:any;
  public modalEdiPublicacion:any;
  public modalListCiudades:any;
  public loading;
  public variedades;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public productosFactory: ProductosFactory, 
    public loadingCtrl: LoadingController,
    public auth: AuthService,
    public toast: Toast,
    private alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public modal: ModalController,
    public searchFactory: SearchFactory,
    public departamentosFactory: DepartamentosFactory,
    public viewCtrl: ViewController


    ) {

    this.variedades = [];
    let municipios = [];

    this.page          = 0;
    this.pageSize      = 10;
    this.moredata      = true;
    this.resultados    = 0;
    this.publicaciones = [];
    this.categorias    = [];
    this.subcategorias = [];
    this.imagenes = [];
    this.ciudadesSeleccionadas = [];


    searchFactory
    .LoadCategories()
    .subscribe(
      (result: any) =>{
        console.log(result);
        this.categorias = result.rows;
        //vm.subcategorias = vm.categorias[0].SubCategoria;
        this.subcategorias = this.categorias;
      },err => {
        console.log('error');
      });


    this.departamentosFactory
    .CargarDepartamentos($rootScope.user.Pai.Id.toString())
    .subscribe(
      res => {
        console.log(res);

        this.Departamentos = res;
      }, 
      err => {
        console.log('error on local data saved');
      });  

    this.CargarVariedades ();

    this.modalNvaPublicacion = this.modal.create('NuevapublicacionPage');
    
    this.modalListCiudades   = this.modal.create('ListaciudadesPage');


    
    this.cargarSiguientePagina(null);
    


}

// $scope.$on('$destroy', function() {
    // vm.modalNvaPublicacion.remove();
    // vm.modalEdiPublicacion.remove();
// });

EditarPublicacion (publicacion, $index) {

  this.imagenes       = publicacion.Imagens;
  this.publicacion    = publicacion;
  this.municipioId    = publicacion.MunicipioId;
  this.departamentoId = null;

  this.modalEdiPublicacion = 
  this.modal.create('EditarpublicacionPage',{imagenes:this.imagenes, publicacion:this.publicacion, municipioId: this.municipioId, Departamentos: this.Departamentos, subcategoria: this.subcategoria, variedades: this.variedades});
  this.modalEdiPublicacion.present();
};

cargarPublicacion(Id) {
  this.navCtrl.push('PublicacionPage', {Id: Id});
}

// ActualizarPublicacion () {

//   this.publicacion.ImagenPrincipal = 'Imagen principal';
//   this.publicacion.Visitas = 0;
//   this.publicacion.MeGusta = 0;
//   this.publicacion.TipoProducto = 'P';
//   this.publicacion.VariedadId = this.variedad.Id;
//   this.publicacion.UsuarioId = this.auth.getPayload().sub.Id;
//   this.publicacion.MunicipioId = this.municipioId;

//   let imagesObservable = [];

//   this.loading = this.loadingCtrl.create({
//     spinner: 'hide',
//     content: "estamos actualizando tu publicación... ",
//     duration: 3000
//   });

//   this.loading.present();

//   if (typeof this.publicacion.Precio == 'undefined') {
//     this.publicacion.Precio = 0;
//   } else {
//     let precio = this.publicacion.Precio.toString();
//     this.publicacion.Precio = precio.replace(",","");
//   }

//   if (typeof this.publicacion.PrecioDesc == 'undefined') {
//     this.publicacion.PrecioDesc=0;
//   }else{
//     let precioDesc = this.publicacion.PrecioDesc.toString();
//     this.publicacion.PrecioDesc = precioDesc.replace(",","");
//   }

//   if (typeof this.publicacion.PorcentajePromo == 'undefined') {
//     this.publicacion.PorcentajePromo=0;
//   }else{
//     let porcentajePromo = this.publicacion.PorcentajePromo.toString();
//     this.publicacion.PorcentajePromo = porcentajePromo.replace("%","");
//   }

//   this.productosFactory
//   .ActualizarPublicacion(this.publicacion.Id, this.publicacion)
//   .flatMap((result: any) => {
//     this.publicacion = result;

//     [].forEach.call(this.imagenes, img => {
//       let observable = new Observable(observer => {
//         this.productosFactory
//         .GuardarImagen(this.publicacion, img.RutaGrande)
//         .then(data => observer.next(data))
//         .catch(error => observer.error(error));
//       });

//       imagesObservable.push(observable);
//     });

//     return Observable.zip(imagesObservable);
//   })
//   .flatMap((result: any) => {
//     this.publicacion.ImagenPrincipal = result[0].response.RutaGrande;

//     return this .productosFactory.ActualizarPublicacion(this.publicacion.Id, this.publicacion);
//   })
//   .subscribe(
//     (result: any) =>{

//       this.loading.dismiss();
//       this.cerrarNvaPublicacion();
//       this.RefrescarInformacion(null);
//       this.toast.show('Felicitaciones su publicación ha editada exitosamente!!!','2500','bottom');
//     },
//     err => {
//       console.log('error');
//       this.loading.dismiss();
//       this.toast.show(err,'2500','bottom');

//     }
//     );
// }

EliminarPublicacion (publicacion, $index) {

  let actionSheet = this.actionSheetCtrl.create({
    title: 'Confirmación',
    buttons: [
    {
      text: 'Eliminar',
      handler: () => {
        console.log('Archive clicked');
        this.productosFactory
        .EliminarPublicacion(publicacion.Id)
        .subscribe(result => {
          console.log("result:",result);
          this.publicaciones.splice($index, 1);
          this.toast.show('Publicación eliminada.','2500','bottom').subscribe(
            toast => {
              console.log(toast);
            }
            );
        },
        err => {
          this.toast.show(err,'2500','bottom');
        });
      }
    },{
      text: 'Cancel',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }
    ]
  });
  actionSheet.present();
}


cargarPagina () {
  let user = this.auth.getPayload().sub;

  return this.productosFactory
  .LoadPublicationListByUser(
    user.Id,
    this.page,
    this.pageSize,
    'createdAt',
    'DESC',''
    );
};


cargarSiguientePagina (infiniteScroll) {
  this.page += 1;

  this.cargarPagina()
  .subscribe((result:any) => {
    let pubs = result.rows;

    console.log(pubs);

    this.moredata      = pubs.length == this.pageSize;
    this.resultados    = result.count;
    this.publicaciones = this.publicaciones.concat(pubs);

    if (!!infiniteScroll) {
      infiniteScroll.complete();
    }
    console.log(result.count);
  },
  err => {
    console.log(err);
  })            
}

RefrescarInformacion (ionRefresher) {
  this.page = 1;
  this.publicaciones = [];
  this.cargarPagina()
  .subscribe((result:any) => {
    let pubs = result.rows;

    this.moredata      = pubs.length == this.pageSize;
    this.resultados    = result.count;
    this.publicaciones = this.publicaciones.concat(pubs);

    ionRefresher.complete();
  },
  err => {
    console.log(err); 
  });
}

CrearPublicacion () {
  this.imagenes       = [];
  this.publicacion    = {};
  this.municipioId    = null;
  this.Departamentos;  
  this.departamentoId = null;
  this.subcategoria   = null;
  this.variedad       = null;

  this.modalNvaPublicacion.present();
};

cerrarNvaPublicacion () {

  //this.modalNvaPublicacion.hide();
};

cerrarEdiPublicacion () {

  //this.modalEdiPublicacion.hide();
};

CargarPagina (page) {

  //$ionicSlideBoxDelegate.slide(page);
};



CargarMunicipios () {
  let departamento = [].filter.call(this.Departamentos, (dep) => {
    return dep.Id == this.departamentoId;
  });

  if (departamento.length > 0) {
    this.Municipios = departamento[0].Municipios;
  } else {
    this.Municipios = [];
  }
    console.log(this.Municipios[0].Id);
    this.municipioId = this.Municipios[0].Id.toString();
  };

CargarVariedades () {
    let cat = [].filter.call(this.subcategorias, function(sub) {
      return sub.Id /*== this.subcategoria*/;
    });

    if (cat.length > 0) {
      this.variedades = cat[0].Variedads;
    } else {
      this.variedades = [];
    }
    this.variedad = null;
  };



// GuardarPublicacion () {
//   if (this.ciudadesSeleccionadas.length==0) {
//     this.toast.show('Por favor seleccione al menos una Ciudad.','2500','bottom');
//   } else {
//     this.publicacion.ImagenPrincipal = 'Imagen principal';
//     this.publicacion.Visitas = 0;
//     this.publicacion.MeGusta = 0;
//     this.publicacion.TipoProducto = 'P';
//     this.publicacion.VariedadId = this.variedad.Id;
//     this.publicacion.UsuarioId = this.auth.getPayload().sub.Id;
//     this.publicacion.MunicipioId = this.municipioId;
//     this.publicacion.Estado = "1";

//     let imagesObservable = [];
//     let citiesObservable = [];

//     this.loading = this.loadingCtrl.create({
//       spinner: 'hide',
//       content: "Estamos guardando tu publicación.... ",
//       duration: 3000

//     });

//     this.loading.present();

//     if (typeof this.publicacion.Precio == 'undefined') {
//       this.publicacion.Precio=0;
//     } else {
//       var precio = this.publicacion.Precio.toString();
//       this.publicacion.Precio = precio.replace(",","");
//     }

//     if (typeof this.publicacion.PrecioDesc == 'undefined') {
//       this.publicacion.PrecioDesc=0;
//     } else {
//       var precioDesc = this.publicacion.PrecioDesc.toString();
//       this.publicacion.PrecioDesc = precioDesc.replace(",","");
//     }

//     if (typeof this.publicacion.PorcentajePromo == 'undefined') {
//       this.publicacion.PorcentajePromo=0;
//     } else {
//       var porcentajePromo = this.publicacion.PorcentajePromo.toString();
//       this.publicacion.PorcentajePromo = porcentajePromo.replace("%","");
//     }
    
//     this.publicacion.NoMeGusta = 0;

//     this.productosFactory
//     .GuardarPublicacion(this.publicacion)
//     .flatMap((result: any) => {
//       this.publicacion = result;

//       [].forEach.call(this.imagenes, img => {
//         let observable = new Observable(observer => {
//           this.productosFactory
//           .GuardarImagen(this.publicacion, img.RutaGrande)
//           .then(data => observer.next(data))
//           .catch(error => observer.error(error));
//         });

//         imagesObservable.push(observable);
//       });

//       [].forEach.call(this.ciudadesSeleccionadas, param =>{
//         let observable = this.productosFactory.GuardarProductoMunicipio({
//           ProductoId: this.publicacion.Id,
//           MunicipioId: param.Id
//         });

//         citiesObservable.push(observable);
//       });

//       Observable.zip(citiesObservable);

//       return Observable.zip(imagesObservable);
//     })
//     .flatMap((result: any) => {
//       this.publicacion.ImagenPrincipal = result[0].response.RutaGrande;

//       return this .productosFactory.ActualizarPublicacion(this.publicacion.Id, this.publicacion);
//     })
//     .subscribe(data => {
//       this.loading.dismiss();
//       this.cerrarNvaPublicacion();
//       this.RefrescarInformacion(null);
//       this.ciudadesSeleccionadas = [];
//       this.terminos = false;
//       this.toast.show('Felicitaciones su publicación ha sido exitosa!!!','2500','bottom');
//     },
//     error => {
//       console.log(error);
//       this.loading.dismiss();
//       this.toast.show(error,'2500','bottom');      
//     });
//   }
// }




error() {
  console.warn('Camera permission is not turned on');
}

success( status ) {
  if( !status.hasPermission ) this.error();

  var options = {
    maximumImagesCount: 6,
    width: 800,
    height: 800,
    quality: 80
  };

  this.imagenes = [];

/*$cordovaImagePicker.getPictures(options)
.then(function (results) {

[].forEach.call(results, function(res) {
  this.imagenes.push({
    RutaGrande: res,
    RutaPequeño: res
  });
});
}, function(error) {
// error getting photos
});*/
}

EscogerCiudades (){


  let departamento = [].filter.call(this.Departamentos, dep => {
    return dep.Id == this.departamentoId;
  });

  if (departamento.length > 0) {
    this.Municipios = departamento[0].Municipios;
  } else {
    this.Municipios = [];
  }
  this.modalListCiudades.present();

}

cerrarNvaListaCiudades (){
  this.modalListCiudades.hide();
}

seleccionarCiudad (item){
  this.ciudadesSeleccionadas.push(item);
  
}

}