import { NgModule } 				from '@angular/core';
import { IonicPageModule } 			from 'ionic-angular';
import { NuevapublicacionPage } 	from './nuevapublicacion';
import { SafeHtml }                 from '../../providers/safeHtml';





@NgModule({
  declarations: [
    NuevapublicacionPage,
    SafeHtml   
    
  ],
  imports: [
    IonicPageModule.forChild(NuevapublicacionPage),
    //SafeHtml

    
  ],
  exports: [
    NuevapublicacionPage,
    SafeHtml
    
  ]
})
export class NuevapublicacionPageModule {}
