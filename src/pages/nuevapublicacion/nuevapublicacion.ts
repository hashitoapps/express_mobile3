import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import { AuthService }                         from 'ng2-ui-auth';
import { ProductosFactory }                    from '../../providers/productos-factory';
import { LoadingController }                          from 'ionic-angular';
import { Toast }                               from '@ionic-native/toast';
import { AlertController }                     from 'ionic-angular';
import { SearchFactory }                       from '../../providers/search-factory';
import { Camera, CameraOptions }               from '@ionic-native/camera';
import { ActionSheetController }               from 'ionic-angular';
import { ActionSheet, ActionSheetOptions }     from '@ionic-native/action-sheet';
import {  ModalController}                     from 'ionic-angular';
import { DepartamentosFactory }                from '../../providers/departamentos-factory';
import { Observable }                          from 'rxjs/Observable';
//import { MyCurrencyFormatterDirective }        from '../../providers/MyCurrencyFormatterDirective';
//para recortar
import { Crop }                                from '@ionic-native/crop';
import { Platform }                            from 'ionic-angular';
import { Base64 }                               from '@ionic-native/base64';
import { SafeHtml }                from '../../providers/safeHtml';
import 'rxjs';
import * as rootScope            from '../../app/rootScope';
import { ModalRecorte} from '../modalseltiporecorte/modalseltiporecorte';


const $rootScope = rootScope.default;

/**
* Generated class for the NuevapublicacionPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-nuevapublicacion',
  templateUrl: 'nuevapublicacion.html',


})
export class NuevapublicacionPage {


  public page;
  public pageSize;
  public moredata;
  public resultados;
  public publicaciones;
  public categorias;
  public subcategorias;
  public variedades;
  public imagenes;
  public ciudadesSeleccionadas;
  public publicacion;
  public municipioId;
  public Departamentos;
  public departamentoId = null;
  public variedad;
  public subcategoria;
  public Municipios;
  public terminos: Boolean;
  public modalNvaPublicacion:any;
  public modalEdiPublicacion:any;
  public modalListCiudades:any;
  public loading;
  public RutaGrande;
  public RutaPequeño;
  public seleccionaTodosMunicipios:any;
   public modalRecorte:any;



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private view: ViewController,
    public productosFactory: ProductosFactory,
    public loadingCtrl: LoadingController,
    public auth: AuthService,
    public toast: Toast,
    private alertCtrl: AlertController,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public modal: ModalController,
    public searchFactory: SearchFactory,
    public departamentosFactory: DepartamentosFactory,
    private actionSheet: ActionSheet,
    private crop:Crop,
    private platform:Platform,
    private base64:Base64




  ) {

    this.variedades     = [];
    this. Municipios = [];

    this.publicacion = {};
    this.page          = 0;
    this.pageSize      = 10;
    this.moredata      = true;
    this.resultados    = 0;
    this.publicaciones = [];
    this.categorias    = [];
    this.subcategorias = [];
    this.imagenes = [];
    this.RutaGrande = [];
    //this.Departamentos = [];

    this.ciudadesSeleccionadas = [];


    this.searchFactory
    .LoadCategories()
    .subscribe(
      (result: any) =>{
        console.log(result);
        this.categorias = result.rows;
        //vm.subcategorias = vm.categorias[0].SubCategoria;
        this.subcategorias = this.categorias;
      },err => {
        console.log('error');
      });


    this.departamentosFactory
    .CargarDepartamentos($rootScope.user.Pai.Id.toString())
    .subscribe(
      res => {
        console.log(res);

        this.Departamentos = res;
        console.log(this.Departamentos.Nombre)
      },
      err => {
        console.log('error on local data saved');
      });
    this.modalListCiudades   = this.modal.create('ListaciudadesPage');
    this.modalRecorte = this.modal.create(ModalRecorte);

  }

  cerrarModal(){
    this.view.dismiss();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad NuevapublicacionPage');
  }

  cargarPublicacion(Id) {
    this.navCtrl.push('PublicacionPage', {Id: Id});
  }

  SeleccionaTodosMunicipiosCheck(){
    console.log("ha seleccionado todos"+this.seleccionaTodosMunicipios);

    if(this.seleccionaTodosMunicipios){
       this.ciudadesSeleccionadas = [];
      for(let entry of this.Municipios){
        console.log(entry);
        this.ciudadesSeleccionadas.push(entry);
      }
    }else{
      this.ciudadesSeleccionadas = [];
    }

  }

 /* ActualizarPublicacion () {

    this.publicacion.ImagenPrincipal = 'Imagen principal';
    this.publicacion.Visitas = 0;
    this.publicacion.MeGusta = 0;
    this.publicacion.TipoProducto = 'P';
    this.publicacion.VariedadId  = this.subcategoria;
    this.publicacion.UsuarioId   = this.auth.getPayload().sub.Id;
    this.publicacion.MunicipioId = this.municipioId;

    let imagesObservable = [];

    this.loading = this.loadingCtrl.create({
      content: "estamos actualizando tu publicación... "
    });

    this.loading.present();

    if (typeof this.publicacion.Precio == 'undefined') {
      this.publicacion.Precio = 0;
    } else {
      let precio = this.publicacion.Precio.toString();
      this.publicacion.Precio = precio.replace(",","");
    }

    if (typeof this.publicacion.PrecioDesc == 'undefined') {
      this.publicacion.PrecioDesc=0;
    }else{
      let precioDesc = this.publicacion.PrecioDesc.toString();
      this.publicacion.PrecioDesc = precioDesc.replace(",","");
    }

    if (typeof this.publicacion.PorcentajePromo == 'undefined') {
      this.publicacion.PorcentajePromo=0;
    }else{
      let porcentajePromo = this.publicacion.PorcentajePromo.toString();
      this.publicacion.PorcentajePromo = porcentajePromo.replace("%","");
    }

    this.productosFactory
    .ActualizarPublicacion(this.publicacion.Id, this.publicacion)
    .flatMap((result: any) => {
      this.publicacion = result;

      [].forEach.call(this.imagenes, img => {
        let observable = new Observable(observer => {
          this.productosFactory
          .GuardarImagen(this.publicacion, img.RutaGrande)
          .then(data => observer.next(data))
          .catch(error => observer.error(error));
        });

        imagesObservable.push(observable);
      });

      return Observable.zip.apply(null, imagesObservable);

    })
    .flatMap((result: any) => {
      this.publicacion.ImagenPrincipal = result[0].response.RutaGrande;

      return this.productosFactory.ActualizarPublicacion(this.publicacion.Id, this.publicacion);
    })
    .subscribe(
      (result: any) =>{

        this.loading.dismiss();
        //this.cerrarNvaPublicacion();
        this.RefrescarInformacion(null);
        this.toast.show('Felicitaciones su publicación ha editada exitosamente!!!','2500','bottom');
      },
      err => {
        console.log('error');
        this.loading.dismiss();
        this.toast.show(err,'2500','bottom');

      }
      );
  }*/



  /*EliminarPublicacion (publicacion, $index) {

    let buttonLabels = ['ELiminar'];

    const opt: ActionSheetOptions = {
      title: '<ion-icon name="trash"></ion-icon>Eliminar',
      subtitle: 'Choose an action',
      buttonLabels: buttonLabels,
      addCancelButtonWithLabel: 'Cancel',
      addDestructiveButtonWithLabel: 'Delete',
      androidTheme: this.actionSheet.ANDROID_THEMES.THEME_HOLO_DARK,
      destructiveButtonLast: true
    };

    this.actionSheet.show(opt).then((buttonIndex: number) => {
      console.log('Button pressed: ' + buttonIndex);
      this.productosFactory
      .EliminarPublicacion(publicacion.Id)
      .subscribe(result => {
        this.publicaciones.splice($index, 1);
        this.toast.show('Publicación eliminada.','2500','bottom');
      },
      err => {
        this.toast.show(err,'2500','bottom');
      });});
  }*/

  cargarPagina () {
    let user = this.auth.getPayload().sub;

    return this.productosFactory
    .LoadPublicationListByUser(
      user.Id,
      this.page,
      this.pageSize,
      'createdAt',
      'DESC',''
      );
  };


  cargarSiguientePagina (infiniteScroll) {
    this.page += 1;

    this.cargarPagina()
    .subscribe((result:any) => {
      let pubs = result.rows;

      console.log(pubs);

      this.moredata      = pubs.length == this.pageSize;
      this.resultados    = result.count;
      this.publicaciones = this.publicaciones.concat(pubs);

      if (!!infiniteScroll) {
        infiniteScroll.complete();
      }
      console.log(result.count);
    },
    err => {
      console.log(err);
    })
  }

  RefrescarInformacion (ionRefresher) {
    this.page = 1;
    this.publicaciones = [];
    this.cargarPagina()
    .subscribe((result:any) => {
      let pubs = result.rows;

      this.moredata      = pubs.length == this.pageSize;
      this.resultados    = result.count;
      this.publicaciones = this.publicaciones.concat(pubs);

     if(!!ionRefresher) {
      ionRefresher.complete();
    }
    },
    err => {
      console.log(err);
    });
  }



CargarMunicipios () {
   let departamento = [].filter.call(this.Departamentos, (dep) => {
    return dep.Id == this.departamentoId;
  });

  if (departamento.length > 0) {
    this.Municipios = departamento[0].Municipios;
    for(let i = 0; i <= departamento.length; i++){

     //this.ciudadesSeleccionadas.push(departamento[i].Municipios);
  }

  } else {
    this.Municipios = [];
  }
  //console.log(vm.Municipios);
  //setTimeout(function(){
    console.log(this.Municipios[0].Id);

    this.municipioId = this.Municipios[0].Id.toString();



    //},1)

  };



  CargarVariedades () {
    let cat = [].filter.call(this.subcategorias, function(sub) {
      return sub.Id /*== this.subcategoria*/;
    });

    if (cat.length > 0) {
      this.variedades = cat[0].Variedads;
    } else {
      this.variedades = [];
    }

    this.variedad=cat[0].Variedads[0]
    console.log("variedad seleccionada -----------"+this.variedad);
    //this.variedad = null;
  };



  GuardarPublicacion () {

    if(!this.seleccionaTodosMunicipios){
        this.ciudadesSeleccionadas = [].concat(this.municipioId).map(mun => { return {Id: mun} });
    }


    console.log("Ciudades Seleccionadas:");
    console.log(this.ciudadesSeleccionadas);
    console.log(this.imagenes);

    if (this.ciudadesSeleccionadas.length==0) {
      this.toast.show('Por favor seleccione al menos una Ciudad.','2500','bottom')
      .subscribe(
        toast => {
          console.log(toast);
        }
        );
    } else {
      this.publicacion.ImagenPrincipal = 'Imagen principal';
      this.publicacion.Visitas = 0;
      this.publicacion.MeGusta = 0;
      this.publicacion.TipoProducto = 'P';
      this.publicacion.VariedadId = this.variedad.Id;
      this.publicacion.UsuarioId = this.auth.getPayload().sub.Id;
      this.publicacion.MunicipioId = this.ciudadesSeleccionadas[0].Id;
      this.publicacion.Estado = "1";

      console.log(this.publicacion);

      let imagesObservable = [];
      let citiesObservable = [];

      this.loading = this.loadingCtrl.create({
        content: "Estamos guardando tu publicación.... "

      });

      this.loading.present();

      if (typeof this.publicacion.Precio == 'undefined') {
        this.publicacion.Precio=0;
      } else {
        let precio = this.publicacion.Precio.toString();
        this.publicacion.Precio = precio.replace(",","");
      }

      if (typeof this.publicacion.PrecioDesc == 'undefined') {
        this.publicacion.PrecioDesc=0;
      } else {
        let precioDesc = this.publicacion.PrecioDesc.toString();
        this.publicacion.PrecioDesc = precioDesc.replace(",","");
      }

      if (typeof this.publicacion.PorcentajePromo == 'undefined') {
        this.publicacion.PorcentajePromo=0;
      } else {
        let porcentajePromo = this.publicacion.PorcentajePromo.toString();
        this.publicacion.PorcentajePromo = porcentajePromo.replace("%","");
      }

      //mientras tanto
      this.publicacion.NoMeGusta = 0;

      this.productosFactory
      .GuardarPublicacion(this.publicacion)
      .flatMap((result: any) => {
        this.publicacion = result;
        console.log("result:", result);

        [].forEach.call(this.ciudadesSeleccionadas, param =>{
          let observable = this.productosFactory.GuardarProductoMunicipio({
            ProductoId: this.publicacion.Id,
            MunicipioId: param.Id
          });

          console.log('Ciudad Id', param.Id);

          citiesObservable.push(observable);
        });

        console.log('Guardando Ciudades', citiesObservable);

        return Observable.zip.apply(null, citiesObservable);
      })
      .flatMap(result => {
        [].forEach.call(this.imagenes, img => {
          imagesObservable.push(this.productosFactory.GuardarImagen(this.publicacion, img.RutaGrande));
        });

        console.log('guradando imagenes: ', imagesObservable);
        return Observable.zip.apply(null, imagesObservable);

      })
      .flatMap((result: any) => {
        console.log("HOLA:", result);

        this.publicacion.ImagenPrincipal = result[0].RutaGrande;
        // for(let i=0 ; i < this.publicacion.length; i++)
        // console.log("IMAGENES:",this.publicacion.imagens[i]);

        return this.productosFactory.ActualizarPublicacion(this.publicacion.Id, this.publicacion);
      })
      .subscribe(data => {
        console.log('subscribe:',data);

        this.loading.dismiss();
        //this.cerrarNvaPublicacion();
        //this.RefrescarInformacion(null);
        this.ciudadesSeleccionadas = [];
        this.terminos = false;
        this.toast.show('Felicitaciones su publicación ha sido exitosa!!!','2500','bottom').subscribe(
            toast => {
              console.log(toast);
            }
            );
        this.cerrarModal();
      },
      error => {
        console.log(error);
        this.loading.dismiss();
        this.toast.show(error,'2500','bottom');
      });
      }
  }

/*SeleccionarImagenes = function() {

    //var permissions = cordova.plugins.permissions;
    //console.log(permissions);

    //permissions.hasPermission(permissions.WRITE_EXTERNAL_STORAGE, function( status ){
    //  console.log("status"+status);
    //  if ( status.checkPermission ) {
        console.log("Yes :D ");

         var options = {
             maximumImagesCount: 6,
             width: 800,
             height: 800,
             quality: 80
            };

            vm.imagenes = [];

            $cordovaImagePicker.getPictures(options)
              .then(function (results) {

                [].forEach.call(results, function(res) {
                  vm.imagenes.push({
                    RutaGrande: res,
                    RutaPequeño: res
                  });
                });
              }, function(error) {
                // error getting photos
              });*/


  /*SeleccionarImagenes() {
    if(this.imagenes.length < 6){
      this.camera.getPicture({
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: false,
        correctOrientation: true,
        //destinationType: this.camera.DestinationType.DATA_URL,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        quality: 75,
        saveToPhotoAlbum: false,
        targetWidth: 800,
        targetHeight: 800
      })
      .then(image => {
        console.log("image"+image);

        /*this.imagenes.push({
          RutaGrande: image,
          RutaPequeño: image
        });

        if (this.platform.is('ios')) {
          return image
        } else if (this.platform.is('android')) {
          // Modify fileUri format, may not always be necessary
          image = 'file://' + image;


          //return this.crop.crop(image, { quality: 100 });
        }

      })
      .then((path)=>{

        console.log('Cropped Image Path!: ' + path);

        //conversion
        console.log("conversion");

        let filePath: string = path;
        this.base64.encodeFile(filePath).then((base64File: string) => {

          console.log("--------------------------------------");
          //console.log(base64File);

              this.imagenes.push({
               path: path,
              RutaGrande: base64File,
              RutaPequeño: base64File
            });


              console.log("--------------------------------------");
        }, (err) => {
          console.log("error->>>>>>"+err);
        });



        //return path;
      })
      .catch(error => console.error(error));
    }else{
      this.toast.show('Supera maximo de imagenes permitidas','2500','bottom').subscribe(
        toast => {
          console.log(toast);
        }
        );
    }
  }*/
  SeleccionarImagenes() {
    if(this.imagenes.length < 6){



          this.camera.getPicture({
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: false,
        correctOrientation: true,
        //destinationType: this.camera.DestinationType.DATA_URL,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        quality: 75,
        saveToPhotoAlbum: false,
        targetWidth: 800,
        targetHeight: 800
      })
      .then(image => {
        console.log("image"+image);

        /*this.imagenes.push({
          RutaGrande: image,
          RutaPequeño: image
        });*/

        if (this.platform.is('ios')) {
          return image
        } else if (this.platform.is('android')) {
          // Modify fileUri format, may not always be necessary
          image = 'file://' + image;




          this.modalRecorte.present();

          this.modalRecorte.onDidDismiss(data => {

            //configuracin cuadrado
            let widthRadioP = 2;
            let heightRadioP =2;



            if(data==2){
               widthRadioP = 2;
              heightRadioP =1;
            }else if(data==3){
               widthRadioP = 1;
              heightRadioP =2;
            }

            let options = {
            quality: 75,
            widthRatio:widthRadioP,
            heightRatio:heightRadioP,
            targetWidth:600,
            targetHeight:600
          };

           return this.crop.crop(image, options)
           .then((path)=>{

        console.log('Cropped Image Path!: ' + path);

        //conversion
        console.log("conversion");

        let filePath: string = path;
        this.base64.encodeFile(filePath).then((base64File: string) => {

          console.log("--------------------------------------");
          //console.log(base64File);

              this.imagenes.push({
               path: path,
              RutaGrande: base64File,
              RutaPequeño: base64File
            });


              console.log("--------------------------------------");
        }, (err) => {
          console.log("error->>>>>>"+err);
        });



        //return path;
      })
      .catch(error => console.error(error));


         });



        }

      })






    }else{
      this.toast.show('Supera maximo de imagenes permitidas','2500','bottom').subscribe(
        toast => {
          console.log(toast);
        }
        );
    }
  }

  Remover(img){
    let i:any=0;
    for(let entry of this.imagenes){
      console.log(img+"-----------------"+entry.path);
      if(img==entry.path){

        break;
      }
      i++;
    }
    //console.log("i"+i);
    this.imagenes.splice(i,1);
    console.log("imagenes:"+this.imagenes);
    //this.imagenes.delete(img);
  }





error() {
  console.warn('Camera permission is not turned on');
}

success( status ) {
  if( !status.hasPermission ) this.error();

  var options = {
    maximumImagesCount: 6,
    width: 800,
    height: 800,
    quality: 80
  };

  //this.imagenes = [];

/*$cordovaImagePicker.getPictures(options)
.then(function (results) {

[].forEach.call(results, function(res) {
this.imagenes.push({
RutaGrande: res,
RutaPequeño: res
});
});
}, function(error) {
// error getting photos
});*/
}

EscogerCiudades () {


  let departamento = [].filter.call(this.Departamentos, dep => {
    return dep.Id == this.departamentoId;
  });

  if (departamento.length > 0) {
    this.Municipios = departamento[0].Municipios;
  } else {
    this.Municipios = [];
  }
  this.modalListCiudades.present();

}

cerrarNvaListaCiudades (){
  this.modalListCiudades.hide();
}

seleccionarCiudad (item){
  this.ciudadesSeleccionadas.push(item);
  //console.log("seleccionada:",item," is ",item.checked);


}
}
