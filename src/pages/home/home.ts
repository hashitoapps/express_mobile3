import { Component }             from '@angular/core';
import { IonicPage,
         NavParams,
         NavController,
         MenuController }        from 'ionic-angular';
import { DepartamentosFactory }  from '../../providers/departamentos-factory';
import { SearchFactory }         from '../../providers/search-factory';
import { UserFactory}            from '../../providers/perfil-factory';
import { Toast }                 from '@ionic-native/toast';
import { NgModule }              from '@angular/core';
import { FormsModule }           from '@angular/forms';
import * as rootScope            from '../../app/rootScope';
//para recibir el mensaje
import { Subscription }          from 'rxjs/Subscription';
import { MessageService }        from '../../providers/message-service';
import { AuthService }           from 'ng2-ui-auth';

const $rootScope = rootScope.default;
/**
 * Generated class for the Home page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
 selector: 'page-home',
 templateUrl: 'home.html',
 providers: [DepartamentosFactory],

})
export class Home {
  public categoria;
  public categorias;
  public variedades;
  public subcategorias;
  public municipioId;
  public departamentoId;
  public departamentos;
  public Departamentos;
  public variedad;
  public subcategoria;
  public descripcion;
  public Municipios;
  public banners;
  public usrApp;

  public message: any;
  public subscription: Subscription;

  public paises:any = [];
  public paisSeleccionado:any = {};


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public departamentosFactory: DepartamentosFactory,
    public userFactory: UserFactory,
    public searchFactory: SearchFactory,
    private toast: Toast,
    public messageService: MessageService,
    public auth: AuthService,
  ) {
    this.menu.swipeEnable(true);
    this.categoria      = null;
    this.categorias     = [];
    this.variedades     = [];
    this.subcategorias  = [];
    this.banners        = [];
    this.municipioId    = null;
    this.departamentos  = null;
    this.departamentoId = null;
    this.Departamentos = [];
    this.descripcion = '';
    this.usrApp        = this.auth.getPayload().sub;

    this.subscription = this.messageService.getMessage().subscribe(message => {
      this.message = message;
      console.log(message);
      this.CargarDepartamentos();

    });


    this.userFactory
    .CargarBanners()
    .subscribe(
      res => {

        this.banners = res.rows;
        console.log('banners: ', this.banners);
      },
      err => {
        console.log('error: ', err);
      });

    this.departamentosFactory
      .CargarDepartamentos($rootScope.user.Pai.Id.toString())
      .subscribe(
        res => {
          console.log(res);

          this.Departamentos = res;

          $rootScope.departamentos = this.Departamentos;

          console.log('$rootScope Home: ', $rootScope);
        },
        err => {
          console.log('error on local data saved');
        });

    this.searchFactory
      .LoadCategories()
      .subscribe(
        (res: any) =>{
          console.log(res);
          this.categorias = res.rows;
          //vm.subcategorias = vm.categorias[0].SubCategoria;
          this.subcategorias = this.categorias;

          this.CargarPaises();

        },err => {
          console.log('error');
        });
  }

  onDepChange($event) {
    console.log($event);
  }

   CargarVariedades () {
     let cat = [].filter.call(this.subcategorias, function(sub) {
       return sub.Id /*== this.subcategoria*/;
     });

     if (cat.length > 0) {
       this.variedades = cat[0].Variedads;
     } else {
       this.variedades = [];
     }
     this.variedad=cat[0].Variedads[0]
     console.log("variedad*******"+this.variedad);
   };

   Buscar () {
    console.log("subcategoria", this.subcategoria);
    console.log("descripcion", this.descripcion);
    console.log("departamentoId", this.departamentoId);
    console.log("municipioId", this.municipioId);
    console.log("Variedad", this.variedad);

     if(this.municipioId!=null){

       this.navCtrl.push('Results', {
        IdCategoria: this.categorias[0].Id,
        IdSubCategoria: this.subcategoria,
        IdVariedad: this.subcategoria,
        Descripcion: this.descripcion,
        IdMunicipio: this.municipioId
      });
    } else {

      this.toast.show('Por favor seleccione una ciudad.','2500','bottom').subscribe(
        toast => {
          console.log(toast);
        }
        );

      /*ionicToast.show('Por favor seleccione una ciudad.', 'bottom', false, 2500);*/
    }
  }

  CargarDepartamentos(){
    this.departamentosFactory
      .CargarDepartamentos($rootScope.user.Pai.Id.toString())
      .subscribe(
        res => {
          console.log(res);

          this.Departamentos = res;

          $rootScope.departamentos = this.Departamentos;

          console.log('$rootScope Home: ', $rootScope);
        },
        err => {
          console.log('error on local data saved');
        });

  }

  CargarMunicipios () {
    let departamento = [].filter.call(this.Departamentos, (dep) => {
      return dep.Id == this.departamentoId;
    });

    if (departamento.length > 0) {
      this.Municipios = departamento[0].Municipios;
    } else {
      this.Municipios = [];
    }
    //console.log(vm.Municipios);
    //setTimeout(function(){
      console.log(this.Municipios[0].Id);

      this.municipioId = this.Municipios[0].Id.toString();
      //},1)

    };

    CargarPaises() {
      this.departamentosFactory
      .CargarPaises()
      .subscribe((result: any) => {
        console.log("result"+ result.rows);
        this.paises = result.rows;
        this.paisSeleccionado = $rootScope.user.Pai.Id;
        //console.log("seleccionado:"+this.paisSeleccionado);

    },
    err => console.log("error", err)
    );
  }

  CambiarPais() {
      console.log(this.paisSeleccionado);
      this.departamentoId = null;
      this.municipioId = null;

      this.departamentosFactory
      .CargarDepartamentos(this.paisSeleccionado)
      .subscribe(
        res => {
          console.log(res);

          this.Departamentos = res;

          $rootScope.departamentos = this.Departamentos;

          console.log('$rootScope Home: ', $rootScope);
        },
        err => {
          console.log('error on local data saved');
        });

    }


IrACitas(){
  this.navCtrl.push('MisreservasPage',{usuario: this.usrApp.Id});
}


  /*ionViewDidLoad() {
    console.log('ionViewDidLoad Home');

    //this.departamentosFactory.CargarDepartamentos().subscribe();
  }*/
}
