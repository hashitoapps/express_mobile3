import { Component }                           from '@angular/core';
import { AuthService }                         from 'ng2-ui-auth';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductosFactory }                    from '../../providers/productos-factory';
import { Loading,
  LoadingController }                   from 'ionic-angular';
  import { Toast }                               from '@ionic-native/toast';
  import { AlertController }                     from 'ionic-angular';
  import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
/**
 * Generated class for the PublicacionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
 @Component({
   selector: 'page-publicacion',
   templateUrl: 'publicacion.html',
 })
 export class PublicacionPage {

   public loading: Loading;
   public publicacion;
   public mensaje;
   public usr;
   public videito;
   public frame;
   public proveedor;

   constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public productosFactory: ProductosFactory,
     public loadingCtrl: LoadingController,
     public auth: AuthService,
     public toast: Toast,
     private alertCtrl: AlertController
    ) {

     this.publicacion = { Usuario: {} };
     this.mensaje     = '';
     this.proveedor = {};
     this.usr         = this.auth.getPayload().sub;
     this.videito = '';
     this.frame ='';


     this.loading = this.loadingCtrl.create({
       spinner: 'hide',
       content: "Espera un momento<br>estamos cargando la publicación... ",
       duration: 3000

     });
     this.loading.present();

     console.log("Entro aquiiiii",this.navParams.get('Id'));

     this.productosFactory
     .LoadPublication(this.navParams.get('Id').Id)
     .subscribe(result => {
       this.publicacion = result;
       this.videito = this.publicacion.Usuario.UrlIntroduccion;

       console.log(result);

       this.loading.dismiss();

     },
     error => {
       this.loading.dismiss();
       console.log(error);
     });

    this.frame= '';
    console.log(this.publicacion);
    console.log(this.videito);
    //console.log(this.frame);
   }


   EnviarMensaje () {

     let contactForm = {
       Name:'',
       Email:'',
       Message:''
     };

     contactForm.Name = this.usr.Nombre;
     contactForm.Email = this.usr.Correo;
     contactForm.Message = this.mensaje;


     this.productosFactory
     .SendContactMail(this.publicacion.Id,contactForm)
     .subscribe(result => {
       this.toast.show('Mensaje enviado correctamente.','2500','bottom');

     },
     error => {
       console.log(error);
     });

     this.loading = this.loadingCtrl.create({
       spinner: 'hide',
       content: "Espera un momento<br>estamos enviando tu mensaje... ",
       duration: 3000

     });
     this.loading.present();

     this.timeout();


   };

   AgendarCita(Id, usuario){
     console.log("Datos publicacion", this.publicacion);
       this.navCtrl.push('CitasPage',{Id: this.publicacion.Id, usuario: this.publicacion.Usuario});
   }

   timeout(){
     setTimeout(() => {
       this.mensaje = '';
       this.loading.dismiss();
     },3000);
   }

   MostrarHistorial () {
     let alert = this.alertCtrl.create({
       title: 'Click',
       buttons: ['Dismiss']
     });
     alert.present();

     //alert("Click");
   }

   IrAPublicacionesDelUsuario (){

     this.navCtrl.push('PublicacionesUsuarioPage',{Id: this.publicacion.Usuario.Id, Nombre: this.publicacion.Usuario.Nombre,
       NombreEmpresa: this.publicacion.Usuario.NombreEmpresa,Nit: this.publicacion.Usuario.Nit, Proveedor: this.publicacion.Usuario.Proveedor});
   };

 /*function format(id){
    var valor=$("#"+id).val();
    var x=accounting.formatMoney(valor, "", "0");
    $("#"+id).val(x);
  }*/

}
