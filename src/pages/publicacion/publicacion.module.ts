import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicacionPage } from './publicacion';
import { SafePipe } from './publicacion';

@NgModule({
  declarations: [
    SafePipe,
    PublicacionPage
  ],
  imports: [
    IonicPageModule.forChild(PublicacionPage),
  ],
  exports: [
    PublicacionPage
  ]
})
export class PublicacionPageModule {}
