import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CitasdisponiblesPage } from './citasdisponibles';

@NgModule({
  declarations: [
    CitasdisponiblesPage,
  ],
  imports: [
    IonicPageModule.forChild(CitasdisponiblesPage),
  ],
  exports: [
    CitasdisponiblesPage
  ]
})
export class CitasdisponiblesPageModule {}
