import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserFactory}                          from '../../providers/perfil-factory';
import { Toast }                 from '@ionic-native/toast';
import * as moment from 'moment';
import { AuthService }                         from 'ng2-ui-auth';

/**
 * Generated class for the CitasdisponiblesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-citasdisponibles',
  templateUrl: 'citasdisponibles.html',
})
export class CitasdisponiblesPage {
  public lstCitasDis;
   public banners;
   public cita;
   public unaCita;
   public nombre;
   public fechahorario;
   public listDias;
   public idDia;
   public reserva;
   public usuario;
   public horasistema;
   public horaseleccionada;
   public formatodia;
     public usrApp;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toast: Toast,
    public userFactory: UserFactory,
    public auth: AuthService,) {

      this.lstCitasDis = [];
      this.banners     = [];
      this.cita = null;
      this.unaCita = {};
      this.nombre = "";
      this.fechahorario = null;
      this.listDias =[];
      this.idDia = null;
      this.reserva ={};
      this.usuario = this.navParams.get('param').Usuario;
      this.horasistema = moment().format('H');
      this.formatodia = moment().format('a');
      this.horaseleccionada = {};
      this.usrApp        = this.auth.getPayload().sub;


      this.listDias = [
      {"dia": "Monday","value": 1},
      {"dia": "Tuesday","value": 2},
      {"dia": "Wednesday","value": 3},
      {"dia": "Thursday","value": 4},
      {"dia": "Friday","value": 5},
      {"dia": "Saturday","value": 6},
      {"dia": "Sunday","value": 7 }]

    console.log("entro aqui en citas",this.navParams.get('param'));
    console.log("entro aqui en fecha",this.navParams.get('fecha'));

    let fecha = moment(this.navParams.get('fecha')).format('YYYY-MM-DD');
    let dia = moment(this.navParams.get('fecha')).format('dddd');

    this.fechahorario = fecha;

    for(let data of this.listDias) {
      if(data.dia == dia){
        this.idDia = data.value;
      }else{
       /*this.toast.show('No existen horarios para el dia seleccionado.','2500','bottom').subscribe(
          toast => {
            console.log(toast);
          }
        );*/
      }
    }
    this.CargarCitasDisponibles();
    this.userFactory
    .CargarBanners()
    .subscribe(
      res => {
        this.banners = res.rows;
        console.log('banners: ', this.banners);
      },
      err => {
        console.log('error: ', err);
  });
  }

  CargarCitasDisponibles(){
    console.log("con esto consulta----")
    console.log([this.navParams.get('servicioid'),this.fechahorario,this.idDia].join(','));
    this.userFactory
    .CargarCitas(this.navParams.get('servicioid'),this.fechahorario,this.idDia)
    .subscribe(result => {
      this.lstCitasDis = result;
      console.log(result);
    },
    err => {
      console.log('error on local data saved');
  });
}

CargarUnaCitasDisponibles(){
  console.log("horaro seleccionado", [this.navParams.get('servicioid'),this.idDia,this.cita].join('---'));
  this.userFactory
  .CargarUnaCIta(this.navParams.get('servicioid'),this.idDia,this.cita)
  .subscribe(result => {
    this.unaCita = result;
    //this.nombre = "Hora: " + this.unaCita[0].nombre +"  -  "+ "Precio: $"+this.unaCita[0].precio;
    this.nombre = "Hora: " + this.unaCita[0].nombre;
    console.log("Horario qqqqqqq", this.horaseleccionada);
  },
  err => {
    console.log('error on local data saved');
});
}

ListaReservas(usurio){
    this.navCtrl.push('MisreservasPage',{usuario: this.usuario});
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CitasdisponiblesPage');
  }

  GuardarCitas(usuario){

      if(this.cita != null){
        console.log("horaro seleccionado", this.cita);

        var info = {
        "empresa_cancha_id": this.navParams.get('servicioid'),
         "fecha": this.fechahorario,
         "tipohora_id": this.cita,
         "usuario_id": this.navParams.get('usuarioAppId'),
          "precio": this.unaCita[0].precio
        }
        this.reserva = info;
        console.log("Entidad a guardar",   this.reserva)
        this.userFactory
        .GuardarCita(this.reserva)
        .subscribe(result => {
          this.reserva = result;
        this.toast.show('Su reserva fue guardada exitosamente.','2500','bottom').subscribe(
            toast => {
              console.log(toast);
            });

            this.navCtrl.pop();

            this.nombre = "";
            this.CargarCitasDisponibles();
            this.navCtrl.push('MisreservasPage',{usuario: this.usrApp.Id});
            console.log("horaro seleccionado", this.reserva);
        });

      }else{
        console.log("horario null", this.cita);
        this.toast.show('Por favor seleccione un horario para continuar.','2500','bottom').subscribe(
          toast => {
            console.log(toast);
          }
          );
      }

  }

}
