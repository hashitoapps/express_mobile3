import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Contactenos } from './contactenos';

@NgModule({
  declarations: [
    Contactenos,
  ],
  imports: [
    IonicPageModule.forChild(Contactenos),
  ],
  exports: [
    Contactenos
  ]
})
export class ContactenosModule {}
