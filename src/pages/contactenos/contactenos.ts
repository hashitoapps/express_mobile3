import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { AuthService }   from 'ng2-ui-auth';

/**
 * Generated class for the ContactenosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-contactenos',
  templateUrl: 'contactenos.html',
})
export class Contactenos {
  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
  	public $auth: AuthService
  ) {
  }
}
