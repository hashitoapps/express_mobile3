import { Component }                           from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService }                         from 'ng2-ui-auth';
import { Transfer,FileUploadOptions, 
         TransferObject } 		                 from '@ionic-native/transfer';
import { File } 				  				             from '@ionic-native/file';
import { UserFactory}                          from '../../providers/perfil-factory';
import { Toast }                               from '@ionic-native/toast';
import { Camera, CameraOptions }               from '@ionic-native/camera';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as rootScope            from '../../app/rootScope';
import { DepartamentosFactory }                from '../../providers/departamentos-factory';

import { MessageService }        from '../../providers/message-service';

const $rootScope = rootScope.default;

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  public user:any = { };
  public myCroppedImage;
  public fileTransfer: TransferObject;
  public htmlStr: string = '<div class="divImagen" style="background-image:url({{ user.Avatar }});"></div>'; 
  public paises:any = [];
  public paisSeleccionado:any = {};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public auth: AuthService, 
    private transfer: Transfer, 
    private file: File, 
    public userFactory: UserFactory,
    private toast: Toast,
    private camera: Camera,
    private sanitizer: DomSanitizer,
    public departamentosFactory: DepartamentosFactory,
    public messageService:MessageService

    ) {
    this.user = $rootScope.user;
    console.log('Usuario:', this.user.Avatar);
    this.myCroppedImage = '';

    if(this.user.Identificacion==0){
      this.user.Identificacion = '';
    }
    if(this.user.Direccion=="null"){
      this.user.Direccion = '';
    }
    if(this.user.Telefono==0){
      this.user.Telefono = '';
    }
    if(this.user.TelefonoWhatsApp==0){
      this.user.TelefonoWhatsApp = '';
    }
    if(this.user.UrlIntroduccion=="null"){
      this.user.UrlIntroduccion = '';
    }
    if(this.user.Descripcion=="null"){
      this.user.Descripcion = '';
    }
    if(this.user.NombreEmpresa=="null"){
      this.user.NombreEmpresa = '';
    }
    if(this.user.Nit=="null"){
      this.user.Nit = '';
    }

    this.CargarPaises();

  }

  
  GuardarAvatar(){
    //let usr = this.auth.getPayload().sub;

    this.userFactory
    .GuardarAvatar(this.user, this.user.Avatar)
    .then((result: any) => {
      console.log(result);
      //this.user.Avatar = this.userFactory.UrlAvatar(this.user);
      //this.user.Completo = !!this.user.Identificacion;
      console.log('resultAvatar', result.Avatar);
      this.user.Avatar = result.Avatar;
      this.toast.show('Avatar guardado correctamente.','2500','bottom').subscribe(
        toast => {
          console.log(toast);
        }
        );
    })
    .catch(err => {
      console.log('error');
      this.toast.show('error.data','2500','bottom');
    });

  }

Guardar() {

  if (this.user.UrlIntroduccion.lastIndexOf("embed") != 24 ){
    let videoId = this.user.UrlIntroduccion.split('https://youtu.be/').join('');
    console.log("videoId:", videoId);
    let rel= 'rel=0';
    let embed = 'https://www.youtube.com/embed';
    this.user.UrlIntroduccion = [embed, videoId].join('/');
    this.user.UrlIntroduccion = [this.user.UrlIntroduccion, rel].join('?');
    console.log("Url Transformada: ", this.user.UrlIntroduccion);
  }
  console.log("seleccionado para guardar:", this.paisSeleccionado);
  this.user.PaiId = this.paisSeleccionado;
  this.userFactory
  .Guardar(this.user.Id, this.user)
  .subscribe((result: any) => {
    
    this.user                  = result;
    this.user.Telefono         = this.user.Telefono * 1;
    this.user.Identificacion   = this.user.Identificacion * 1;
    this.user.TelefonoWhatsApp = this.user.TelefonoWhatsApp * 1;

    this.user.Completo         = !!this.user.Nombre &&
    !!this.user.Identificacion &&
    !!this.user.Telefono &&
    !!this.user.TelefonoWhatsApp &&
    !!this.user.Direccion;

    let paisSeleccionadoObjeto = [].find.call(this.paises, (pa) => {
      return pa.Id == this.paisSeleccionado;
    });

    console.log(paisSeleccionadoObjeto.Id);

    //console.log(this.user.Pai.Nombre);
    $rootScope.user = this.user;
    $rootScope.user.Pai = paisSeleccionadoObjeto;

    this.messageService.sendMessage('Message from Home Component to App Component!');

    //$rootScope.paisSeleccionadoId = this.paisSeleccionado;
    this.toast.show('Perfil guardado correctamente.','2500','bottom').subscribe(
        toast => {
          console.log(toast);
        }
     );
    this.navCtrl.pop();
  },
  err => console.log("error", err)
  );
}

cancelar (){
  this.navCtrl.pop();
}

CambiarAvatar () {
  
      this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      quality: 75,
      saveToPhotoAlbum: false,
      targetWidth: 800,
      targetHeight: 800
    })
    .then(image => {
      this.user.Avatar = image;
    })
    .catch(error => console.error(error));
  
};

  CargarPaises() {
      this.departamentosFactory
      .CargarPaises()
      .subscribe((result: any) => {
        console.log("result"+ result.rows); 
        this.paises = result.rows; 
        this.paisSeleccionado = $rootScope.user.Pai.Id;
        //console.log("seleccionado:"+this.paisSeleccionado);    

    },
    err => console.log("error", err)
    );
  }

  CambiarPais() {
      console.log(this.paisSeleccionado);
    }

}
