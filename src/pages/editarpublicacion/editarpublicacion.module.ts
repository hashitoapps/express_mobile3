import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarpublicacionPage } from './editarpublicacion';

@NgModule({
  declarations: [
    EditarpublicacionPage,
  ],
  imports: [
    IonicPageModule.forChild(EditarpublicacionPage),
  ],
  exports: [
    EditarpublicacionPage
  ]
})
export class EditarpublicacionPageModule {}
