import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthService }                         from 'ng2-ui-auth';
import { ProductosFactory }                    from '../../providers/productos-factory';
import { LoadingController }                          from 'ionic-angular';
import { Toast }                               from '@ionic-native/toast';
import { AlertController }                     from 'ionic-angular';
import { SearchFactory }                       from '../../providers/search-factory';
import { Camera, CameraOptions }               from '@ionic-native/camera';
import { ActionSheetController }               from 'ionic-angular';
import { ActionSheet, ActionSheetOptions }     from '@ionic-native/action-sheet';
import { ModalController}                      from 'ionic-angular';
import { DepartamentosFactory }                from '../../providers/departamentos-factory';
import { Observable }                          from 'rxjs/Observable';
import 'rxjs';
import * as rootScope            from '../../app/rootScope';

const $rootScope = rootScope.default;

/**
 * Generated class for the EditarpublicacionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-editarpublicacion',
  templateUrl: 'editarpublicacion.html',
})
export class EditarpublicacionPage {

  public page;
  public pageSize;
  public moredata;
  public resultados;
  public publicaciones;
  public categorias;
  public subcategorias;
  public imagenes;
  public ciudadesSeleccionadas;
  public publicacion;
  public municipioId;
  public Departamentos;
  public departamentoId = null; 
  public variedades;
  public variedad;
  public subcategoria;
  public Municipios;
  public terminos: Boolean;
  public modalNvaPublicacion:any;
  public modalEdiPublicacion:any;
  public modalListCiudades:any;
  public loading;
  public imt:boolean = false;
  public imf:boolean = false;

  public seleccionaTodosMunicipios:any;
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private view: ViewController,
    public productosFactory: ProductosFactory, 
    public loadingCtrl: LoadingController,
    public auth: AuthService,
    public toast: Toast,
    private alertCtrl: AlertController,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public modal: ModalController,
    public searchFactory: SearchFactory,
    public departamentosFactory: DepartamentosFactory,
    private actionSheet: ActionSheet
  ) {
    this.publicacion      = navParams.get('publicacion');
    this.page             = 0;
    this.pageSize         = 10;
    this.moredata         = true;
    this.resultados       = 0;
    this.publicaciones    = [];
    this.categorias       = [];
    this.municipioId      = navParams.get('municipioId')
    this.subcategoria     = navParams.get('subcategoria')
    this.imagenes         = navParams.get('imagenes');
    this.variedad         = navParams.get('variedad'); 
    this.variedades       = navParams.get('variedades')
    this.Municipios       = [];
    this.ciudadesSeleccionadas = [];

    console.log(this.publicacion);
    [].forEach.call(this.imagenes, img => {
      img.fromUrl = img.RutaGrande.lastIndexOf("http") == 0;
    });
      
   searchFactory
    .LoadCategories()
    .subscribe(
      (result: any) =>{
        console.log(result);
        this.categorias = result.rows;
        //vm.subcategorias = vm.categorias[0].SubCategoria;
        this.subcategorias = this.categorias;
      },err => {
        console.log('error');
      });


    departamentosFactory
    .CargarDepartamentos($rootScope.user.Pai.Id.toString())
    .subscribe(
      res => {
        console.log(res);

        this.Departamentos = res;

        this.Departamentos.forEach(dep => {
          let mun:any = dep.Municipios.filter(mun => {
            return mun.Id == this.publicacion.MunicipioId;
          });

          if (mun.length > 0) {
            console.log('Municipio', mun);
            console.log('Municipio', mun[0]);
            this.departamentoId = mun[0].DepartamentoId;
            console.log(this.departamentoId);
          }
        });

        this.CargarMunicipios();

        this.municipioId = [];

        this.publicacion.ProductoMunicipios.forEach(mun => {
          this.municipioId.push(mun.MunicipioId);
        });
      }, 
      err => {
        console.log('error on local data saved');
      });
  

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditarpublicacionPage');
  }

  cerrarModal(){
    this.view.dismiss();
  }

  EditarPublicacion (publicacion, $index) {

    this.imagenes       = publicacion.Imagens;
    this.publicacion    = publicacion;
    this.municipioId    = publicacion.MunicipioId;
    this.departamentoId = null;
    this.subcategoria   = publicacion.Variedad.SubCategoriumId;
    this.variedad       = publicacion.VariedadId;

    //$ionicSlideBoxDelegate.slide(0);

    this.modalEdiPublicacion.show();
  };

  cargarPublicacion(Id) {
    this.navCtrl.push('PublicacionPage', {Id: Id});
  }



ActualizarPublicacion () {
  
  //this.ciudadesSeleccionadas = [].concat(this.municipioId).map(mun => { return {Id: mun} });

  if(!this.seleccionaTodosMunicipios){
     this.ciudadesSeleccionadas = [].concat(this.municipioId).map(mun => { return {Id: mun} });
  }

  this.publicacion.ImagenPrincipal = 'Imagen principal';
  this.publicacion.Visitas = 0;
  this.publicacion.MeGusta = 0;
  this.publicacion.TipoProducto = 'P';
  this.publicacion.VariedadId = this.subcategoria;
  this.publicacion.UsuarioId = this.auth.getPayload().sub.Id;
  this.publicacion.MunicipioId = this.ciudadesSeleccionadas[0].Id;

  let imagesObservable = [];
  let citiesObservable = [];

  this.loading = this.loadingCtrl.create({
    
    content: "estamos actualizando tu publicación... ",
   
  });

  this.loading.present();

  if (typeof this.publicacion.Precio == 'undefined') {
    this.publicacion.Precio = 0;
  } else {
    let precio = this.publicacion.Precio.toString();
    this.publicacion.Precio = precio.replace(",","");
  }

  if (typeof this.publicacion.PrecioDesc == 'undefined') {
    this.publicacion.PrecioDesc=0;
  }else{
    let precioDesc = this.publicacion.PrecioDesc.toString();
    this.publicacion.PrecioDesc = precioDesc.replace(",","");
  }

  if (typeof this.publicacion.PorcentajePromo == 'undefined') {
    this.publicacion.PorcentajePromo=0;
  }else{
    let porcentajePromo = this.publicacion.PorcentajePromo.toString();
    this.publicacion.PorcentajePromo = porcentajePromo.replace("%","");
  }

  this.productosFactory
  .ActualizarPublicacion(this.publicacion.Id, this.publicacion)
  .flatMap((result: any) => {
      //eliminar productomunicipio
    return this.productosFactory.EliminarProductoMunicipioTodos(this.publicacion.Id)
    

    //eliminar productomunicipio
  })
  .flatMap((result: any) => {
    [].forEach.call(this.ciudadesSeleccionadas, param =>{
          let observable = this.productosFactory.GuardarProductoMunicipio({
            ProductoId: this.publicacion.Id,
            MunicipioId: param.Id
          });

          console.log('Ciudad Id', param.Id);

          citiesObservable.push(observable);
        });

        console.log('Guardando Ciudades', citiesObservable);

        return Observable.zip.apply(null, citiesObservable);
  })
  .flatMap((result: any) => {
    this.publicacion = result;
 
    [].forEach.call(this.imagenes, img => {
      if (img.RutaGrande.lastIndexOf("http") != 0 ) {
        imagesObservable.push(this.productosFactory.GuardarImagen(this.publicacion, img.RutaGrande));
      }
    });

    console.log('guradando imagenes: ', imagesObservable);
     return Observable.zip.apply(null, imagesObservable);
  })
  .flatMap((result: any) => {
    console.log('Result: ', result);

    this.publicacion.ImagenPrincipal = result[0].RutaGrande;

    return this.productosFactory.ActualizarPublicacion(this.publicacion.Id, this.publicacion);
  })
  .subscribe(
    (result: any) =>{

      this.loading.dismiss();
      //this.cerrarNvaPublicacion();
      //this.RefrescarInformacion(null);
      this.toast.show('Felicitaciones su publicación ha sido editada exitosamente!!!','2500','bottom').subscribe(
            toast => {
              console.log(toast);
            }
            );
      this.cerrarModal();
    },
    err => {
      console.log('err:', err);
      this.loading.dismiss();
      this.toast.show(err,'2500','bottom');

    }
    );
}


EliminarPublicacion (publicacion, $index) {

  let buttonLabels = ['ELiminar'];

  const opt: ActionSheetOptions = {
    title: '<ion-icon name="trash"></ion-icon>Eliminar',
    subtitle: 'Choose an action',
    buttonLabels: buttonLabels,
    addCancelButtonWithLabel: 'Cancel',
    addDestructiveButtonWithLabel: 'Delete',
    androidTheme: this.actionSheet.ANDROID_THEMES.THEME_HOLO_DARK,
    destructiveButtonLast: true
  };

  this.actionSheet.show(opt).then((buttonIndex: number) => {
    console.log('Button pressed: ' + buttonIndex);
    this.productosFactory
    .EliminarPublicacion(publicacion.Id)
    .subscribe(result => {
      this.publicaciones.splice($index, 1);
      this.toast.show('Publicación eliminada.','2500','bottom');
    },
    err => {
      this.toast.show(err,'2500','bottom');
    });});
}



cargarPagina () {
  let user = this.auth.getPayload().sub;

  return this.productosFactory
  .LoadPublicationListByUser(
    user.Id,
    this.page,
    this.pageSize,
    'createdAt',
    'DESC',''
    );
};


cargarSiguientePagina (infiniteScroll) {
  this.page += 1;

  this.cargarPagina()
  .subscribe((result:any) => {
    let pubs = result.rows;

    console.log(pubs);

    this.moredata      = pubs.length == this.pageSize;
    this.resultados    = result.count;
    this.publicaciones = this.publicaciones.concat(pubs);

    if (!!infiniteScroll) {
      infiniteScroll.complete();
    }
    console.log(result.count);
  },
  err => {
    console.log(err);
  })            
}

RefrescarInformacion (ionRefresher) {
  this.page = 1;
  this.publicaciones = [];
  this.cargarPagina()
  .subscribe((result:any) => {
    let pubs = result.rows;

    this.moredata      = pubs.length == this.pageSize;
    this.resultados    = result.count;
    this.publicaciones = this.publicaciones.concat(pubs);

    ionRefresher.complete();
  },
  err => {
    console.log(err); 
  });
}

/*CrearPublicacion () {
  this.imagenes       = [];
  this.publicacion    = {};
  this.municipioId    = null;
  this.Departamentos;  
  this.departamentoId = null;
  this.subcategoria   = null;
  this.variedad       = null;

  //$ionicSlideBoxDelegate.slide(0);

  this.modalNvaPublicacion.present();
};

cerrarNvaPublicacion () {

  //this.modalNvaPublicacion.hide();
};

cerrarEdiPublicacion () {

  //this.modalEdiPublicacion.hide();
};

CargarPagina (page) {

  //$ionicSlideBoxDelegate.slide(page);
};

*/

CargarMunicipios () {
  let departamento = [].filter.call(this.Departamentos, (dep) => {
    return dep.Id == this.departamentoId;
  });

  if (departamento.length > 0) {
    this.Municipios = departamento[0].Municipios;
  } else {
    this.Municipios = [];
  }
  //console.log(vm.Municipios);
  //setTimeout(function(){
    console.log(this.Municipios[0].Id);

    this.municipioId = this.Municipios[0].Id.toString();
    //},1)

  };




  CargarVariedades () {
    let cat = [].filter.call(this.subcategorias, function(sub) {
      return sub.Id /*== this.subcategoria*/;
    });

    if (cat.length > 0) {
      this.variedades = cat[0].Variedads;
    } else {
      this.variedades = [];
    }
    this.variedad = null;
  };

  SeleccionaTodosMunicipiosCheck(){
    console.log("ha seleccionado todos"+this.seleccionaTodosMunicipios);

    if(this.seleccionaTodosMunicipios){
       this.ciudadesSeleccionadas = [];
      for(let entry of this.Municipios){
        console.log(entry);
        this.ciudadesSeleccionadas.push(entry); 
      }
    }else{
      this.ciudadesSeleccionadas = [];
    }  

  }

// GuardarPublicacion () {
//   if (this.ciudadesSeleccionadas.length==0) {
//     this.toast.show('Por favor seleccione al menos una Ciudad.','2500','bottom');
//     //ionicToast.show('Por favor seleccione al menos una Ciudad.', 'bottom', false, 2500);
//   } else {
//     this.publicacion.ImagenPrincipal = 'Imagen principal';
//     this.publicacion.Visitas = 0;
//     this.publicacion.MeGusta = 0;
//     this.publicacion.TipoProducto = 'P';
//     this.publicacion.VariedadId = this.variedad.Id;
//     this.publicacion.UsuarioId = this.auth.getPayload().sub.Id;
//     this.publicacion.MunicipioId = this.municipioId;
//     this.publicacion.Estado = "1";

//     let imagesObservable = [];
//     let citiesObservable = [];

//     this.loading = this.loadingCtrl.create({
//       spinner: 'hide',
//       content: "Estamos guardando tu publicación.... ",
//       duration: 3000

//     });

//     this.loading.present();

//     if (typeof this.publicacion.Precio == 'undefined') {
//       this.publicacion.Precio=0;
//     } else {
//       var precio = this.publicacion.Precio.toString();
//       this.publicacion.Precio = precio.replace(",","");
//     }

//     if (typeof this.publicacion.PrecioDesc == 'undefined') {
//       this.publicacion.PrecioDesc=0;
//     } else {
//       var precioDesc = this.publicacion.PrecioDesc.toString();
//       this.publicacion.PrecioDesc = precioDesc.replace(",","");
//     }

//     if (typeof this.publicacion.PorcentajePromo == 'undefined') {
//       this.publicacion.PorcentajePromo=0;
//     } else {
//       var porcentajePromo = this.publicacion.PorcentajePromo.toString();
//       this.publicacion.PorcentajePromo = porcentajePromo.replace("%","");
//     }

//     //mientras tanto
//     this.publicacion.NoMeGusta = 0;

//     this.productosFactory
//     .GuardarPublicacion(this.publicacion)
//     .flatMap((result: any) => {
//       this.publicacion = result;

//       [].forEach.call(this.imagenes, img => {
//         let observable = new Observable(observer => {
//           this.productosFactory
//           .GuardarImagen(this.publicacion, img.RutaGrande)
//           .then(data => observer.next(data))
//           .catch(error => observer.error(error));
//         });

//         imagesObservable.push(observable);
//       });

//       [].forEach.call(this.ciudadesSeleccionadas, param =>{
//         let observable = this.productosFactory.GuardarProductoMunicipio({
//           ProductoId: this.publicacion.Id,
//           MunicipioId: param.Id
//         });

//         citiesObservable.push(observable);
//       });

//       Observable.zip(citiesObservable);

//       return Observable.zip(imagesObservable);
//     })
//     .flatMap((result: any) => {
//       this.publicacion.ImagenPrincipal = result[0].response.RutaGrande;

//       return this .productosFactory.ActualizarPublicacion(this.publicacion.Id, this.publicacion);
//     })
//     .subscribe(data => {
//       this.loading.dismiss();
//       //this.cerrarNvaPublicacion();
//       this.RefrescarInformacion(null);
//       this.ciudadesSeleccionadas = [];
//       this.terminos = false;
//       this.toast.show('Felicitaciones su publicación ha sido exitosa!!!','2500','bottom');
//     },
//     error => {
//       console.log(error);
//       this.loading.dismiss();
//       this.toast.show(error,'2500','bottom');      
//     });
//   }
 
//  }


SeleccionarImagenes() {
    if(this.imagenes.length < 6){
      this.camera.getPicture({
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: false,
        correctOrientation: true,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        quality: 75,
        saveToPhotoAlbum: false,
        targetWidth: 800,
        targetHeight: 800
      })
      .then(image => {
        //let maxImage = this.imagenes.length >= 6 ? 1 : 0; 
        this.imagenes.push({
          RutaGrande: image,
          RutaPequeño: image
        });
        
      })
      .catch(error => console.error(error));
    }else{
      this.toast.show('Supera maximo de imagenes permitidas','2500','bottom').subscribe(
        toast => {
          console.log(toast);
        }
        );
    }
  }

error() {
  console.warn('Camera permission is not turned on');
}

success( status ) {
  if( !status.hasPermission ) this.error();

  var options = {
    maximumImagesCount: 6,
    width: 800,
    height: 800,
    quality: 80
  };

  this.imagenes = [];

/*$cordovaImagePicker.getPictures(options)
.then(function (results) {

[].forEach.call(results, function(res) {
this.imagenes.push({
RutaGrande: res,
RutaPequeño: res
});
});
}, function(error) {
// error getting photos
});*/
}

EscogerCiudades (){


  let departamento = [].filter.call(this.Departamentos, dep => {
    return dep.Id == this.departamentoId;
  });

  if (departamento.length > 0) {
    this.Municipios = departamento[0].Municipios;
  } else {
    this.Municipios = [];
  }
  this.modalListCiudades.present();

}

cerrarNvaListaCiudades (){
  this.modalListCiudades.hide();
}

seleccionarCiudad (item){
  this.ciudadesSeleccionadas.push(item);
  //console.log("seleccionada:",item," is ",item.checked);
}
}
