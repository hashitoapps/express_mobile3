import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Loading, 
         LoadingController }                         from 'ionic-angular';
import { ProductosFactory }                          from '../../providers/productos-factory';
/*import { NgModule }                   from '@angular/core';
import { FormsModule }                from '@angular/forms';*/

/**
 * Generated class for the ResultsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class Results {

    public page;
    public pageSize;
    public moredata;
    public resultados;
    public publicaciones;
    public c;


    

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtrl: LoadingController, 
    public productosFactory: ProductosFactory
  ) {

    this.page          = 0;
    this.pageSize      = 10;
    this.moredata      = true;
    this.resultados    = 0;
    this.publicaciones = [];
    //this.c = null;
   
    this.cargarSiguientePagina(null);

  }

  ionViewDidLoad() {
    this.CrearFiltro();
  }

   cargarPagina () {
    return this.productosFactory
     .LoadPublicationList(
        this.page,
        this.pageSize,
        'createdAt',
        'DESC',//c
        this.CrearFiltro()
      );
  }

  cargarPublicacion(Id) {
    this.navCtrl.push('PublicacionPage', {Id: Id});
  }

  cargarSiguientePagina (infiniteScroll) {
    this.page += 1;
     
    this.cargarPagina()
      .subscribe((result:any) => {
        let pubs = result.rows;

        console.log(pubs);

        this.moredata      = pubs.length == this.pageSize;
        this.resultados    = result.count;
        this.publicaciones = this.publicaciones.concat(pubs);

        if (!!infiniteScroll) {
          infiniteScroll.complete();
        }
        console.log(result.count);
      },
      err => {
        console.log(err);
      })            
  }

 RefrescarInformacion (ionRefresher) {
    this.page = 1;
    this.publicaciones = [];
    this.cargarPagina()
      .subscribe((result:any) => {
        let pubs = result.rows;

        this.moredata      = pubs.length == this.pageSize;
        this.resultados    = result.count;
        this.publicaciones = this.publicaciones.concat(pubs);

        ionRefresher.complete();
      },
      err => {
        console.log(err); 
      });
  }

  CrearFiltro() {
    let filter = [];

    console.log("crearfiltro",this.navParams.get('IdCategoria'));
    filter.push('IdCategoria=' + this.navParams.get('IdCategoria'));
    filter.push('IdSubCategoria=' + this.navParams.get('IdSubCategoria'));
    filter.push('IdVariedad=' + this.navParams.get('IdVariedad'));
    filter.push('Descripcion=' + this.navParams.get('Descripcion')) || '';
    filter.push('IdMunicipio=' + this.navParams.get('IdMunicipio'));

    return filter.join('&');
  }

}
