import { Component,
         ViewChild }                  from '@angular/core';
import { AuthService }                from 'ng2-ui-auth';
import { IonicPage,
         NavController,
         NavParams,
         MenuController }             from 'ionic-angular';
import { Toast }                      from '@ionic-native/toast';
import { UserFactory}                 from '../../providers/perfil-factory';
import { Loading,
         LoadingController }          from 'ionic-angular';
import { Slides }                     from 'ionic-angular';
import 'rxjs/Rx';
import { Facebook,
        FacebookLoginResponse }       from '@ionic-native/facebook';
import { Events }                     from 'ionic-angular';
import { NgModule }                   from '@angular/core';
import { FormsModule }                from '@angular/forms';
import * as rootScope                 from '../../app/rootScope';

import { GooglePlus }                 from '@ionic-native/google-plus';
import { NativeStorage }              from '@ionic-native/native-storage';

const $rootScope = rootScope.default;

/**
* Generated class for the SignUp page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUp {
  @ViewChild(Slides) slides: Slides;
  public usr;
  public user;
  public login;
  loading: Loading;
  fbResponse: any;
  public estado;
  public tipoUsuario;
  public infoUser;
  public proveedor;
  public isNewUserFacebook;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public userFactory: UserFactory,
    public toast: Toast,
    public auth: AuthService,
    public loadingCtrl: LoadingController,
    public fb: Facebook,
    public events: Events,
    public googlePlus: GooglePlus,
    public nativeStorage: NativeStorage
  ) {
    this.menu.close();
    this.menu.swipeEnable(false);
    this.usr = {};
    this.user = {};
    this.login ={};
    this.tipoUsuario = null;
    this.infoUser ={};
    this.proveedor = null;
    this.isNewUserFacebook =false;
  }

  MostrarRegistro (esProveedor) {
    this.usr = {
      Proveedor: esProveedor
    };
    this.slides.slideTo(2, 500);
  };

  MostrarInicioSesion () {
    this.slides.slideTo(0, 500);
  };

  MostrarInicio () {
    this.slides.slideTo(1, 500);
  };


  SignUp() {
    console.log("HOLA !!!!!!!!!!!")
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: "Espera un momento<br>estamos guardando tu información... ",
      duration: 3000
    });

    this.loading.present();
    this.auth
    .signup(this.usr)
    .map(res => res.json())
    .flatMap((result: any) => {
      console.log("HOLA:", result);
      this.auth.setToken(result.Token);
      $rootScope.user = this.auth.getPayload().sub;
      this.events.publish('user:logued', this.auth.getPayload().sub);
      return this.auth.login(this.usr);
    })
    .subscribe(result => {
      this.loading.dismiss();
      this.navCtrl.setRoot('Home');
      console.log("Entro aquiiiiiiiiiiiiiii");
      this.GuardarUsuario();

    },
    error => {

      let errorjson = JSON.parse(error._body);

      console.log(errorjson.name);

      if (errorjson.name==='SequelizeUniqueConstraintError') {

        this.usr.ISFACEBOOK=true;
        this.Login();

        this.toast.show('El correo que esta utilizando ya esta registrado.','2500','bottom').subscribe(
          toast => {
            console.log(toast);
          }
          );
      } else {
        this.toast.show('Se ha presentado un error al registrar su usuario.','2500','bottom').subscribe(
          toast => {
            console.log(toast);
          });
      }
    });
  };

  GuardarUsuario(){
    console.log("", this.user);
    if(this.usr.Proveedor == true){
      this.tipoUsuario = 2 ;//EMPRESA
      this.proveedor = 0;

    }else{
      this.proveedor = 1;
      this.tipoUsuario = 3;
    }
    this.user = {
		"Nombre": this.usr.Nombre,
		"Correo": this.usr.Correo,
	  "Clave": this.usr.Clave,
    "Proveedor": this.proveedor
    }
    console.log("usuario para citas", this.user);
    this.userFactory
    .GuardarUsuario(this.user)
      .subscribe((result: any) => {
        this.user = result
        console.log("Usuario final", this.user);
        this.usr.UsuarioCitasId = this.user.id;
        this.userFactory
          .Guardar(this.usr.Id,this.usr)
          .subscribe((result: any)=> {
            console.log("listo grabado en citas");
            this.usr = result;
            this.usr.ISFACEBOOK = this.login.ISFACEBOOK;
            this.Login();
          })

      },
      error => {
        console.log(error);
      });
  }

  Login () {
    this.loading = this.loadingCtrl.create({
      /*spinner: 'hide',*/
      content: "Espera un momento<br>estamos guardando tu información... "
      /*duration: 3000*/

    });

    // this.usr.ISFACEBOOK=false;
    console.log(this.usr)
    this.auth
      .login(this.usr)
      .map(res => res.json())
      .subscribe((result: any) => {
        console.log(result);
        this.auth.setToken(result.Token);
        this.loading.dismiss();

        $rootScope.user = this.auth.getPayload().sub;
        this.events.publish('user:logued', this.auth.getPayload().sub);
        this.navCtrl.setRoot('Home');

        if(this.usr.ISFACEBOOK) {
          this.navCtrl.push('PerfilPage');
        }
      },
      error => {
        console.log(error);
        this.loading.dismiss();
        this.toast.show('Se presento un error al iniciar sesión :'+error._body ,'2500','bottom').subscribe(
          toast => {
            console.log(toast);
          }
          );;
      });
    }

  loginFB() {
    this.fb
      .login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        this.fbResponse = res;
        if(res.status == 'connected') {
          return this.getUserFacebook();
        } else {

          return this.connectFacebook();
        }
      })
      .then((user: any) => {
        console.log('User FB: ', user);

        this.login.name        = user.first_name + ' ' + user.last_name ;
        this.login.foto        = user.picture.data.url;

        if(user.email){
          this.usr.email = user.email;
          console.log(this.login.email);
        }else{
          this.usr.email = [user.id,'@','facebook.com'].join('');
          console.log(this.usr.email);
        }

        this.login.ISFACEBOOK  = true;
        this.login.facebook_id = user.id;

        this.userFactory.GoToFacebook({access_token: this.fbResponse.authResponse.accessToken})
          .flatMap(result => {
            console.log('GoToFacebook', result);

            result.Proveedor = this.usr.Proveedor;

            return this.userFactory.Guardar(result.Id, result);
          })
          .subscribe(result => {
              console.log('Guardar', result);
              this.usr = result;
              this.usr.ISFACEBOOK  = true;

              //Guardar usuario en citas

              this.GuardarUsuario();
              //this.Login();

            },
            error => console.error(error)
          );
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }

  getUserFacebook() {
    return this.fb
    .api("me?fields=id,first_name,last_name,picture,email", ["email"])
    .then((result) => {
      console.log(result);
      this.estado = 'connected';
      //if(!result.email) {
        //this.fb
        //.api('/me/permissions',['DELETE'])
        //.then((err) => console.log(err))

        //throw new Error("No dispone del email");
      //}
      return result;
    });
  }


  connectFacebook() {
    return this.fb
    .login(["public_profile", "email"])
    .then((success) => { return this.getUserFacebook() });
  }

  doGoogleLogin(){
      let nav = this.navCtrl;
      let env = this;
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      this.googlePlus.login({
        'scopes': '', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        'webClientId': '259140863635-7gh06uhaevf7fgge32bj1jgo94io7bth.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        'offline': true
      })
      .then(function (user) {
        console.log("se autentico con google"+user.email+user.displayName+user.imageUrl);
        loading.dismiss()


        //guardo los datos del usuario y envio a crearlo
        env.usr.Correo = user.email;
        env.usr.Nombre = user.displayName;
        env.usr.Clave  = user.email;



        env.nativeStorage.setItem('user', {
          name: user.displayName,
          email: user.email,
          picture: user.imageUrl
        })
        .then(function(){
          //nav.push('Home');
          env.SignUp();
        }, function (error) {
          console.log(error);
        })
      }, function (error) {
        console.log("ocurrió un error en el logueo"+error);
        loading.dismiss();
      });
  }






}
