var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavParams, IonicPage, NavController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Facebook } from '@ionic-native/facebook';
/**
 * Generated class for the Test page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Test = (function () {
    function Test(navCtrl, navParams, camera, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.fb = fb;
        this.images = [];
    }
    Test.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Test');
    };
    Test.prototype.loginFB = function () {
        var _this = this;
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            console.log('Logged into Facebook!', res);
            _this.fbResponse = res;
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    Test.prototype.seleccionarImagenes = function () {
        var _this = this;
        this.camera.getPicture({
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: true,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            quality: 75,
            saveToPhotoAlbum: false,
            targetWidth: 2048,
            targetHeight: 2048
        })
            .then(function (image) {
            _this.images.push(image);
        })
            .catch(function (error) { return console.error(error); });
    };
    return Test;
}());
Test = __decorate([
    IonicPage(),
    Component({
        selector: 'page-test',
        templateUrl: 'test.html',
    }),
    __metadata("design:paramtypes", [NavController,
        NavParams,
        Camera,
        Facebook])
], Test);
export { Test };
//# sourceMappingURL=test.js.map