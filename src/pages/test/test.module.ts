import { NgModule }        from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Camera }          from '@ionic-native/camera';
import { Facebook }        from '@ionic-native/facebook';
import { Test }            from './test';

@NgModule({
  declarations: [
    Test,
  ],
  imports: [
    IonicPageModule.forChild(Test),
  ],
  exports: [
    Test
  ],
  providers: [
    Camera,
    Facebook
  ]
})
export class TestModule {}
