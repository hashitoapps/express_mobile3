import { Component }             from '@angular/core';
import { NavParams,
         IonicPage,
         NavController }         from 'ionic-angular';
import { Camera }                from '@ionic-native/camera';
import { Facebook,
         FacebookLoginResponse } from '@ionic-native/facebook';
/**
 * Generated class for the Test page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class Test {
  images: Array<any>;
  fbResponse: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public camera: Camera,
    public fb: Facebook
  ) {
    this.images = [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Test');
  }

  loginFB() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        this.fbResponse = res;
      })
      .catch(e => console.log('Error logging into Facebook', e));  
  }

  seleccionarImagenes() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      quality: 75,
      saveToPhotoAlbum: false,
      targetWidth: 2048,
      targetHeight: 2048
    })
    .then(image => {
      this.images.push(image);
    })
    .catch(error => console.error(error));
  }
}
