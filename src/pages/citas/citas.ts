import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Loading,LoadingController }           from 'ionic-angular';
import { ProductosFactory }                    from '../../providers/productos-factory';
import { SearchFactory }         from '../../providers/search-factory';
import { AuthService }                         from 'ng2-ui-auth';
import { Transfer,FileUploadOptions,
         TransferObject } 		                 from '@ionic-native/transfer';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Toast }                 from '@ionic-native/toast';
import { UserFactory}            from '../../providers/perfil-factory';
import * as moment from 'moment';

/**
 * Generated class for the CitasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-citas',
  templateUrl: 'citas.html',
})
export class CitasPage {
   public publicacion;
   public loading: Loading;
   public subcategoria;
   public variedades;
   public variedad;
   public categoria;
   public categorias;
   public subcategorias;
   public fechaPedido;
   public citas;
   public banners;
  public miListaReservas;
    public fechaSistema;
    public listaServicios;
    public servicio;
    public usrApp;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public productosFactory: ProductosFactory,
    public loadingCtrl: LoadingController,
    public searchFactory: SearchFactory,
    public userFactory: UserFactory,
    private toast: Toast,
    public auth: AuthService,) {

       this.publicacion = { Usuario: {} };
       this.variedades     = [];
       this.subcategorias  = [];
       this.categoria      = null;
       this.categorias     = [];
       this.banners        = [];
       this.fechaPedido = new Date().toISOString();
       this.citas = null;
       this.miListaReservas = [];
       this.listaServicios = [];
       this.servicio = {};
       this.usrApp        = this.auth.getPayload().sub;

       this.fechaSistema = new Date().toISOString();

       console.log("Fecha sistema", this.fechaSistema);

       this.loading = this.loadingCtrl.create({
         spinner: 'hide',
         content: "Espera un momento<br>estamos cargando la publicación... ",
         duration: 3000

       });
      this.loading.present();

      console.log("entro aqui en citas",this.navParams.get('Id'));
      console.log("entro aqui en citas",this.navParams.get('usuario'));

      let user = this.navParams.get('usuario');

      this.productosFactory
      .LoadPublication(this.navParams.get('Id'))
      .subscribe(result => {
        this.publicacion = result;

        console.log(result);

        this.loading.dismiss();

      },
      error => {
        this.loading.dismiss();
        console.log(error);
      });
      this.searchFactory
        .LoadCategories()
        .subscribe(
          (res: any) =>{
            console.log(res);
            this.categorias = res.rows;
            //vm.subcategorias = vm.categorias[0].SubCategoria;
            this.subcategorias = this.categorias;
          },err => {
            console.log('error');
      });
      this.userFactory
      .CargarBanners()
      .subscribe(
        res => {

          this.banners = res.rows;
          console.log('banners: ', this.banners);
        },
        err => {
          console.log('error: ', err);
    });

    this.userFactory
      .CargarListaServicios(user.UsuarioCitasId)
      .subscribe(
        res => {
          console.log(res);
          this.listaServicios = res;
        },
        err => {
          console.log("no se pudo traer lista de servicios");
        }
      );
  }

  ConsultarReservas(){
    console.log(this.usrApp);
    this.userFactory
    .CargarCitasReservadas(this.usrApp.Id)
      .subscribe(result => {
        this.miListaReservas = result;
        console.log("Mi lista Reserca", this.miListaReservas);

        if(this.miListaReservas.length <= 0){
          console.log("Mi lista esta vacia mostrar mensaje");
         this.toast.show('No hay citas reservadas para mostrar.','2500','bottom').subscribe(
              toast => {
                console.log(toast);
              });
        }else{
          this.navCtrl.push('MisreservasPage',{usuario: this.usrApp.Id});
        }
      },
        err => {
          console.log('error on local data saved');
      });
  }

  BuscarCitas(param, fecha){

    let fechadia1 = moment(this.fechaPedido).format('YYYY-MM-DD');
    let fechadia2 = moment(this.fechaSistema).format('YYYY-MM-DD');

    if(fechadia1  >= fechadia2 ){
        console.log("La fecha seleccionada correctamente sigueinte proceso");
        if(this.fechaPedido != null && this.servicio!=null){
          console.log(this.servicio);
          this.navCtrl.push('CitasdisponiblesPage',{param: this.publicacion, fecha: this.fechaPedido, servicioid: this.servicio, usuarioAppId:this.usrApp.Id});
        }else{
          console.log("Fecha pedido null", this.fechaPedido);
          this.toast.show('Por favor seleccione una fecha para realizar la busqueda.','2500','bottom').subscribe(
            toast => {
              console.log(toast);
            }
            );
        }
      }else{
          console.log("La fecha de reserva debe ser mayor o igual a la fecha de hoy");
          this.toast.show('La fecha de reserva debe ser mayor o igual a la fecha de hoy.','2500','bottom').subscribe(
              toast => {
                console.log(toast);
              });
      }
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad CitasPage');
  }

}
