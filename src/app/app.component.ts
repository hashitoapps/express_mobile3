import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Menu }  from 'ionic-angular';
import { StatusBar }            from '@ionic-native/status-bar';
import { SplashScreen }         from '@ionic-native/splash-screen';
import { AuthService }          from 'ng2-ui-auth';
import { UserFactory}           from '../providers/perfil-factory';
import { Events }               from 'ionic-angular';
import { Terminos }             from '../pages/terminos/terminos';
import { AlertController }     from 'ionic-angular';
import * as rootScope           from './rootScope';
import { NativeStorage }        from '@ionic-native/native-storage';


const $rootScope = rootScope.default;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'SignUp';
  user: any;

  pages: Array<{title: string, component: any, icon: string, proveedorRequired: boolean, rootPage: boolean}>;
  auth: AuthService;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public auth2: AuthService,
    public userFactory: UserFactory,
    public alertCtrl: AlertController,
    public events: Events,
    public nativeStorage: NativeStorage
  ) {
    this.auth = auth2;
    this.initializeApp();

    this.user = { Proveedor: false };

    this.events.subscribe('user:logued', (user) => {
      this.user = $rootScope.user;
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio',                      component: 'Home',                   icon: 'home',           proveedorRequired: false, rootPage: true },
      // { title: 'Test',          component: 'Test',                   icon: 'home', proveedorRequired: false, rootPage: false },
      { title: 'Mi cuenta',                   component: 'PerfilPage',             icon: 'contact',        proveedorRequired: false, rootPage: false },
      { title: 'Publicaciones',               component: 'AdminPublicacionesPage', icon: 'book',           proveedorRequired: true,  rootPage: false },
      { title: 'Términos y condiciones',      component:  Terminos,                icon: 'help-circle',    proveedorRequired: false, rootPage: false },
      { title: 'Contactenos',                 component: 'Contactenos',            icon: 'pin',            proveedorRequired: false, rootPage: false },
      
    ];

    console.log(this.auth.isAuthenticated());
    let env = this;

    if (this.auth.isAuthenticated()) {
      this.user = $rootScope.user = this.auth.getPayload().sub;
      
      userFactory
        .CargarUsuario($rootScope.user.Id)
        .subscribe(result => {
          let user: any = result;
          user.Completo = 
            !!user.Identificacion &&
            !!user.Telefono &&
            !!user.TelefonoWhatsApp && 
            !!user.Direccion;

          user.Telefono         = user.Telefono * 1;
          user.Identificacion   = user.Identificacion * 1;
          user.TelefonoWhatsApp = user.TelefonoWhatsApp * 1;

          this.user = $rootScope.user = user;

          console.log('$rootScope: ', $rootScope);
        },
        error => {
          console.log(error);
        });
    }

        this.rootPage = !this.auth.isAuthenticated() ? 'SignUp' : 'Home';

    /*}else{//la parte de Google
        
        env.nativeStorage.getItem('user')
        .then( function (data) {
          // user is previously logged and we have his data
          // we will let him access the app
          //env.nav.push(UserPage);
          //env.splashscreen.hide();
          console.log("si se autentico"+data);
          env.rootPage = 'Home';
        }, function (error) {
          //we don't have the user data so we will ask him to log in
          //env.nav.push(LoginPage);
          //this.splashscreen.hide();
          console.log("no se autentico"+error);
          env.rootPage = 'SignUp';
        });

    }*/

    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.platform.registerBackButtonAction(() => {
        if(this.nav._views.length == 1) {
          let alert = this.alertCtrl.create({
            title: 'Confirmación',
            message: '¿Desea salir de la aplicación?',
            buttons: [{
              text: 'Aceptar',
              handler: () => this.platform.exitApp()
            }, {
              text: 'Cancelar',
              role: 'cancel'
            }]
          });

          alert.present();
        } else {
          this.nav.pop();
        }
      });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.rootPage) {
      this.nav.setRoot(page.component);
    } else {
      this.nav.push(page.component);
    }
  }

  CerrarSesion(){
    console.log("cerro sesion");
    this.auth.logout();
    this.nav.setRoot('SignUp');
  }
}
