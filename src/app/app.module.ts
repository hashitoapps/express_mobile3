import { BrowserModule }                       from '@angular/platform-browser';
import { ErrorHandler }                        from '@angular/core';
import { NgModule }                            from '@angular/core';
import { IonicApp }                            from 'ionic-angular';
import { IonicModule }                         from 'ionic-angular';
import { IonicErrorHandler }                   from 'ionic-angular';

import { Ng2UiAuthModule }                     from 'ng2-ui-auth';
import { CustomConfig }                        from 'ng2-ui-auth';

import { MyApp }                               from './app.component';

import { StatusBar }                           from '@ionic-native/status-bar';
import { SplashScreen }                        from '@ionic-native/splash-screen';
import { Config }                              from '../providers/config';
import { Transfer,
         FileUploadOptions,
         TransferObject }                      from '@ionic-native/transfer';
import { File }                                from'@ionic-native/file';
import { LoadingController }                   from 'ionic-angular';
import { Toast }                               from '@ionic-native/toast';
import { UserFactory }                         from '../providers/perfil-factory';
import { ProductosFactory }                    from '../providers/productos-factory';
import { Facebook,
         FacebookLoginResponse }               from '@ionic-native/facebook';
import {SearchFactory}                         from '../providers/search-factory';
import { ActionSheet, ActionSheetOptions }     from '@ionic-native/action-sheet';
import { DepartamentosFactory }                from '../providers/departamentos-factory';
import { Camera, CameraOptions }               from '@ionic-native/camera';
import { Terminos }                            from '../pages/terminos/terminos';

import { NativeStorage }        from '@ionic-native/native-storage';
import { GooglePlus }                 from '@ionic-native/google-plus';
import { MessageService }                      from '../providers/message-service';
//import { MyCurrencyFormatterDirective }        from '../providers/MyCurrencyFormatterDirective';
//import { MyCurrencyPipe } from "../providers/my-currency.pipe";

import { Crop }                                 from '@ionic-native/crop';
import { Base64 }                               from '@ionic-native/base64';
import { ModalRecorte} from '../pages/modalseltiporecorte/modalseltiporecorte';


//import { CategoriasFactoryProvider } from '../providers/categorias-factory';



export class MyAuthConfig extends CustomConfig {
  loginUrl       = [Config.ApiUrl, 'usuarios/login'].join('/');
  signupUrl      = [Config.ApiUrl, 'usuarios/signup'].join('/');
  tokenName      = 'Token';
  tokenPrefix    = 'prevenirExpress';

  defaultHeaders = {'Content-Type': 'application/json'};
}

@NgModule({
  declarations: [
    MyApp,
    Terminos,
    ModalRecorte


  ],
  //exports:[SafeHtml],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {scrollAssist: false, autoFocusAssist: false }),
    Ng2UiAuthModule.forRoot(MyAuthConfig),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Terminos,
    ModalRecorte

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Transfer,
    TransferObject,
    File,
    Toast,
    UserFactory,
    Facebook,
    SearchFactory,
    ProductosFactory,
    DepartamentosFactory,
    LoadingController,
    Camera,
    ActionSheet,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NativeStorage,
    GooglePlus,
    MessageService,
    //MyCurrencyFormatterDirective,
    //MyCurrencyPipe,
    Crop,
    Base64



    //RootScopeProvider,
    //CategoriasFactoryProvider,
    //PerfilFactoryProvider
  ]
})
export class AppModule {}
